import QtQuick 2.4
import QtQuick.Controls 1.3

Rectangle {
    id: root
    property alias arrowDirection: root.direction
    property alias arrowOpacity: root.opacity
    property string direction: "left"
    signal clicked

    width: opacity ? 40 : 0
    anchors.left: if (direction === "left") parent.left
    anchors.right: if (direction === "right") parent.right
    anchors.margins: 10
    opacity: 1
    anchors.verticalCenter: parent.verticalCenter
    antialiasing: true
    height: 40
    radius: 4
    color: mouse.pressed ? "#222" : "transparent"

    Behavior on opacity { NumberAnimation{} }
    Image {
        anchors.verticalCenter: parent.verticalCenter
        source: direction === "left" ? "../images/arrow_left.png" : "../images/arrow_right.png"
    }
    MouseArea {
        id: mouse
        anchors.fill: parent
        anchors.margins: -10
        onClicked: {
            if (root.opacity) {
                root.clicked();
            }
        }
    }
}
