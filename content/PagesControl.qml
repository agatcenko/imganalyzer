import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2

Rectangle {
    id: pageSelector

    property int customHeight
    property alias codecsModel: codecsComboBox.model
    property alias displayCodecs: codecsComboBox.visible

    width: parent.width
    height: customHeight
    color: "lightgray"
    border.color: "black"
    radius: 3
    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        Text {
            text: "Current page: " + FileContent.currentPage
            font.pixelSize: parent.height / 2
        }
        Rectangle {
            width: 100
            height: customHeight
            color: "transparent"
            border.color: "black"
            Text {
                text: "Previous"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    FileContent.previousPage();
                }
            }
        }
        Rectangle {
            width: 100
            height: parent.height
            color: "transparent"
            border.color: "black"
            Text {
                text: "Next"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    FileContent.nextPage();
                }
            }
        }

        ComboBox {
            id: codecsComboBox
            visible: false
            model: []
            onCurrentTextChanged: {
                FileString.currentCodecChanged(currentText);
            }
            width: 100
            height: parent.height
            style: ComboBoxStyle {
                background: Rectangle {
                    border.width: 2
                    color: control.pressed ? "#888" : "transparent"
                }
                label: Text {
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "black"
                    text: control.currentText
                    elide: Text.ElideRight
                }
            }
        }

        Text {
            anchors.leftMargin: 15
            text: "Total pages: " + FileContent.totalPages
            font.pixelSize: parent.height / 2
        }
        Text {
            text: FileContent.readStatusText
            color: FileContent.readStatusColor
        }
    }
}
