import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: welcomeWindow
    width: 450
    height: 350
    visible: true

    Rectangle {
        color: "gray"
        anchors.fill: parent
    }

    toolBar: BorderImage {
        border.bottom: 8
        source: "../images/toolbar.png"
        width: parent.width
        height: 88

        Arrow {
            id: backButton
            arrowDirection: "left"
            arrowOpacity: stackView.depth > 1 ? 1 : 0
            onClicked: {
                stackView.pop();
                if (stackView.depth === 1) {
                    windowTitle.text = "Welcome"
                }
            }
        }

        Arrow {
            id: nextButton
            arrowDirection: "right"
            arrowOpacity: stackView.currentItem.nextPage !== "" && stackView.depth > 1 ? 1 : 0
            onClicked: {
                stackView.push(Qt.resolvedUrl(stackView.currentItem.nextPage));
            }
        }

        Text {
            id: windowTitle
            font.pixelSize: 42
            Behavior on x { NumberAnimation { easing.type: Easing.OutCubic } }
            x: backButton.x + backButton.width + 20
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
            text: "Welcome"
        }
    }

    ListModel {
        id: menuModel
        ListElement {
            title: "New Case"
            page: "NewCaseFirst.qml"
        }
        ListElement {
            title: "Last Case"
            page: "LastCase.qml"
        }
        ListElement {
            title: "Recent Cases"
            page: "RecentCases.qml"
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: parent.width
            height: parent.height
            color: "black"
            opacity: 0.5
            y: list.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 1.5
                    damping: 0.2
                }
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        focus: true
        Keys.onPressed: if (stackView.depth > 1) {
                            if (event.key === Qt.Key_Left) {
                                stackView.pop();
                                event.accepted = true;
                                if (stackView.depth === 1) {
                                    windowTitle.text = "Welcome"
                                }
                            }
                            else if (event.key === Qt.Key_Right && stackView.currentItem.nextPage !== "") {
                                stackView.push(Qt.resolvedUrl(stackView.currentItem.nextPage));
                                event.accepted = true;
                            }
                         }
        initialItem: Item {
            width: parent.width
            height: parent.height
            ListView {
                id: list
                model: menuModel
                anchors.fill: parent

                delegate: ElementsDelegate {
                    text: title
                    onClicked: {
                        stackView.push(Qt.resolvedUrl(page));
                        windowTitle.text = title;
                    }
                    Keys.onPressed: if (event.key === Qt.Key_Right && stackView.depth === 1) {
                                        event.accepted = true;
                                        stackView.push(Qt.resolvedUrl(page));
                                        windowTitle.text = title;
                                    }
                }
                highlight: highlight
                focus: true
            }
        }
    }
}
