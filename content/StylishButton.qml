import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Button {
    id: button
    style: Component {
        id: buttonStyle
        ButtonStyle {
            background: Rectangle {
                anchors.fill: parent
                implicitHeight: 30
                implicitWidth: 100
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#1e92b2" }
                    GradientStop { position: 1 ; color: "#126d9c" }
                }
            }
            label: Text {
                text: control.text
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "white"
                font.pixelSize: 23
                renderType: Text.NativeRendering
            }
        }
    }
}

