import QtQuick 2.4

Rectangle {
    id: root

    width: parent.width
    height: 1
    color: "transparent"

    signal positionChanged
    property int difference: mouseArea.mouseY - mouseArea.oldMouseY

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        property int oldMouseY
        hoverEnabled: true

        cursorShape: "SizeVerCursor"

        onPressed: {
            oldMouseY = mouseY
        }

        onPositionChanged: {
            if (pressed) {
                root.positionChanged()
            }
        }
    }
}
