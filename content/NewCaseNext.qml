import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

import CppCases 1.0

Item {
    width: parent.width
    height: parent.height
    property int margin: 15
    property string nextPage: ""

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: margin
        visible: true

        InputField {
            id: pathField
            text: "Choose Image"

            StylishButton {
                text: "Browse"
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: 10
                onClicked: fileDialog.open();
            }
        }

        InputField {
            id: nameField
            text: "Choose Name"
        }

        RowLayout {
            width: parent.width
            height: parent.height
            Label {
                width: 310
                id: warningText
                color: "#9d0c0c"
                font.pixelSize: 15
            }

            CheckBox {
                id: rewriteCheckBox
                text: "Rewrite Flag"
                anchors.right: createButton.left
                style: CheckBoxStyle {
                    indicator: Rectangle {
                        implicitWidth: 16
                        implicitHeight: 16
                        radius: 3
                        border.color: control.activeFocus ? "darkblue" : "gray"
                        border.width: 1
                        Rectangle {
                            visible: control.checked
                            color: "#555"
                            border.color: "#333"
                            radius: 1
                            anchors.margins: 4
                            anchors.fill: parent
                        }
                    }
                    label: Text {
                        color: "white"
                        text: control.text
                    }
                }

                visible: false
            }

           StylishButton {
                id: createButton
                anchors.right: parent.right
                text: "Create"
                onClicked: {
                    if (newCase.isAllFilled()) {
                        if (!newCase.createCase()) {
                            warningText.text =
                                    "Such case is already exists!\n" +
                                    " - change name or \n" +
                                    " - set \'Rewrite Flag\'";
                            console.log("Database with such name is already exists");
                            rewriteCheckBox.visible = true;
                        }
                        else {
                            mainLayout.visible = false;
                            shadowRect.visible = true;
                        }
                    }
                    else {
                        warningText.text = "You should fill all fields!";
                        console.log("Something is not filled!");
                    }
                }
            }
        }
    }

    Rectangle {
        id: shadowRect
        visible: false
        anchors.fill: parent
        color: "gray"
        Spinner {
            id: spinner
            anchors.centerIn: parent
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        nameFilters: "Disk files (*.dd *.img)"
        onAccepted: {
            var path = fileDialog.fileUrl.toString();
            path = path.replace(/^(file:\/{2})|(qrc:\/{2})|(http:\/{2})/,"");
            var cleanPath = decodeURIComponent(path);
            pathField.inputText = cleanPath;
        }
    }

    NewCase {
        id: newCase
        path: pathField.inputText
        name: nameField.inputText
        rewriteFlag: rewriteCheckBox.checked

        onCaseCreated: {
            console.log("The case has been created");
            toolsBuilder.connect();
            var component = Qt.createComponent("MainWindow.qml");
            var window = component.createObject(root);
            window.show();
            welcomeWindow.hide();
        }
        onNameChanged: console.log("New name: " + newCase.name)
        onPathChanged: {
            console.log("New path: " + newCase.path);
            nameField.inputText = newCase.nameFromPath(newCase.path);
        }
        onRewriteFlagChanged: console.log("Rewrite flag state changed")
    }
}
