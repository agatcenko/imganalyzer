import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Rectangle {
    id: root
    property alias text: label.text
    property alias inputText: inputText.text

    width: parent.width
    height: 88
    color: "gray"
    border.color: Qt.darker(color)

    Text {
        id: label
        text: "Input Field:"
        width: parent.width
        height: 30
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10
        color: "white"
        font.family: "Ubuntu"
        font.pixelSize: 28
    }

    TextField {
        id: inputText
        anchors.top: label.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 10
        style: inputStyle
    }

    Component {
        id: inputStyle
        TextFieldStyle {
            textColor: "white"
            font.pixelSize: 22
            background: Item {
                BorderImage {
                    border.left: 8
                    border.right: 8
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    source: "../images/textinput.png"
                }
            }
        }
    }
}
