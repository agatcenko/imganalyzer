import QtQuick 2.4
import QtQuick.Controls 1.4

Rectangle {
    property alias labelText: label.text
    property alias inputText: filterInput.text
    property alias validator: filterInput.validator
    property alias validatorPass: addFilterParams.enabled
    property alias inputPlaceholder: filterInput.placeholder

    signal addClicked

    id: filter
    height: 40
    border.color: "black"
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 5
    radius: 5

    Rectangle {
        anchors.fill: parent
        anchors.margins: 5

        Label {
            id: label
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            text: "Filter Name"
            font.bold: true
            font.pixelSize: 16
        }

        CustomTextInput {
            id: filterInput
            width: 200
            height: parent.height
            anchors.left: label.right
            anchors.right: addFilterParams.left
            placeholder: "Input"
        }

        AddButton {
            id: addFilterParams
            anchors.right: parent.right
            enabled: filterInput.text.length
            onClicked: {
                filter.addClicked()
                filterInput.accepted()
            }
        }
    }
}
