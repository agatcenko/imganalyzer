import QtQuick 2.4
import QtQuick.Controls 1.4

Item {
    id: root

    Column {
        anchors.fill: parent
        spacing: 2
        anchors.topMargin: 5


        PagesControl {
            id: pagesControl
            customHeight: 30
            codecsModel: FileString.codecsList
            displayCodecs: true
        }

        TextArea {
            id: textArea
            width: parent.width
            height: parent.height - pagesControl.customHeight
            text: FileString.content
        }
    }
}
