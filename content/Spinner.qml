import QtQuick 2.3


AnimatedSprite {
    id: root
    width: 106
    height: 106
    source: "../images/spinner_sprite.png"
    frameCount: 19
    frameRate: 19
    frameWidth: 106
    frameHeight: 106
    loops: AnimatedSprite.Infinite
}
