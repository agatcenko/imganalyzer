import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1

Item {
    id: root

    property int fileId;

    onFileIdChanged: {
        FileInfo.getInfo(fileId);
        FileContent.readFileContent(fileId, 1);
    }

    TabView {
        id: tabs

        anchors.fill: parent
        anchors.top: parent.top
        anchors.margins: 5
        tabPosition: Qt.TopEdge

        Tab {
            title: "Meta"

            ScrollView {
                ColumnLayout {
                    GroupBox {
                        Grid {
                            rows: 3
                            columns: 2
                            spacing: 5
                            anchors.fill: parent
                            anchors.topMargin: 5
                            anchors.leftMargin: 10
                            anchors.rightMargin: 10

                            Label { text: "Name: " }
                            Text { text: FileInfo.name }

                            Label { text: "Md5 hash: " }
                            Text { text: FileInfo.hash }

                            Label { text: "Location: " }
                            Text { text: FileInfo.path }

                        }
                    }
                    GroupBox {
                        Layout.fillWidth: true

                        GridLayout {
                            flow: GridLayout.LeftToRight
                            columns: 3
                            anchors.fill: parent
                            Layout.fillWidth: true
                            anchors.right: parent.right

                            GroupBox {
                                Layout.fillWidth: true
                                Layout.minimumWidth: 300
                                Layout.maximumWidth: 300

                                GridLayout {
                                    flow:  GridLayout.TopToBottom
                                    rows: 4

                                    Label { text: "Created : " }
                                    Label { text: "Changed : " }
                                    Label { text: "Accessed: " }
                                    Label { text: "Modified: " }

                                    Text { text: FileInfo.crtime }
                                    Text { text: FileInfo.ctime }
                                    Text { text: FileInfo.atime }
                                    Text { text: FileInfo.mtime }
                                }
                            }
                            GroupBox {
                                Layout.fillWidth: true
                                Layout.minimumWidth: 300
                                Layout.maximumWidth: 300

                                GridLayout {
                                    flow:  GridLayout.TopToBottom
                                    rows: 4

                                    Label { text: "Size: " }
                                    Label { text: "Mode: " }
                                    Label { text: "UID : " }
                                    Label { text: "GID : " }

                                    Text { text: FileInfo.size }
                                    Text { text: FileInfo.metaMode }
                                    Text { text: FileInfo.uid }
                                    Text { text: FileInfo.gid }
                                }
                            }
                            GroupBox {
                                Layout.fillWidth: true

                                GridLayout {
                                    flow:  GridLayout.TopToBottom
                                    rows: 4

                                    Label { text: "Type (Dir)  : " }
                                    Label { text: "Type (Meta) : " }
                                    Label { text: "Flag (Dir)  : " }
                                    Label { text: "Flag (Meta) : " }

                                    Text { }
                                    Text { text: FileInfo.metaType }
                                    Text { }
                                    Text { text: FileInfo.metaFlag }
                                }
                            }
                        }
                    }
                }
            }
        }

        Tab {
            title: "Hex View"
            anchors.fill: parent

            HexView {
                id: hexViewer
                hexModel: FileHex
                anchors.fill: parent
            }
        }

        Tab {
            title: "String View"
            anchors.fill: parent

            StringView {}
        }

        Tab {
            title: "Extract"
            anchors.fill: parent
            anchors.topMargin: 10

            ExtractView {
                currentFileId: fileId
            }
        }

        style: TabViewStyle {
            frameOverlap: 1
            tab: Rectangle {
                implicitHeight: 20
                implicitWidth: Math.max(tabText.width + 4, 100)
                border.color: "black"
                color: styleData.selected ? "black" : "white"
                Text {
                    id: tabText
                    anchors.centerIn: parent
                    text: styleData.title
                    color: styleData.selected ? "white" : "black"
                }
            }
            frame: Rectangle { color: "transparent" }
            tabsAlignment: Qt.AlignHCenter
        }
    }
}
