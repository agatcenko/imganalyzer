import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    id: root

    property variant dataModel

    RowLayout {
        id: rowLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        Rectangle {
            id: searchParams

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.maximumWidth: Math.max(rowLayout.width * 0.5, 300)
            border.color: "gray"
            radius: 5

            Rectangle {
                id: timeParams
                height: 40
                border.color: "black"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 5
                radius: 5

                property var datetimePattern: /^(\d{4}-(0[^0]|1[0-2])-(0[^0]|[12]\d|3[01])) (([01]\d|2[0-3]):[0-5]\d:[0-5]\d)$/

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 5

                    Label {
                        id: timeLabel
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        text: "Time"
                        font.bold: true
                        font.pixelSize: 16
                    }

                    ComboBox {
                        id: timeType
                        width: 100
                        height: parent.height
                        anchors.left: timeLabel.right
                        model: ["Created", "Changed", "Modified", "Accessed"]
                    }

                    CustomTextInput {
                        id: fromTime
                        width: 160
                        height: parent.height
                        anchors.left: timeType.right
                        placeholder: "YYYY-MM-DD HH:MM:SS"
                        validator: RegExpValidator { regExp: timeParams.datetimePattern}
                    }

                    CustomTextInput {
                        id: tillTime
                        width: 160
                        height: parent.height
                        anchors.left: fromTime.right
                        anchors.right: addTime.left
                        placeholder: "YYYY-MM-DD HH:MM:SS"
                        validator: RegExpValidator { regExp: timeParams.datetimePattern }
                    }

                    AddButton {
                        id: addTime

                        anchors.right: parent.right
                        enabled: {
                            timeParams.datetimePattern.test(fromTime.text) &&
                            timeParams.datetimePattern.test(tillTime.text)
                        }
                        onClicked: {
                            dataModel.addTimeFilter(timeType.currentText, fromTime.text, tillTime.text)
                            fromTime.accepted()
                            tillTime.accepted()
                        }
                    }
                }
            }

            Rectangle {
                id: sizeParams
                height: 40
                border.color: "black"
                radius: 5

                anchors.top: timeParams.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 5

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 5

                    Label {
                        id: sizeLabel
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        text: "Size"
                        font.bold: true
                        font.pixelSize: 16
                    }

                    CustomTextInput {
                        id: fromSize
                        width: 200
                        height: parent.height
                        anchors.left: sizeLabel.right
                        placeholder: "From (bytes)"
                        validator: IntValidator { bottom: 0 }
                    }

                    CustomTextInput {
                        id: tillSize
                        width: 200
                        height: parent.height
                        anchors.left: fromSize.right
                        anchors.right: addSizeParams.left
                        placeholder: "Till (bytes)"
                        validator: IntValidator { bottom: 0 }
                    }

                    AddButton {
                        id: addSizeParams
                        anchors.right: parent.right
                        enabled: fromSize.text.length && tillSize.text.length
                        onClicked: {
                            dataModel.addSizeFilter(fromSize.text, tillSize.text)
                            fromSize.accepted()
                            tillSize.accepted()
                        }
                    }
                }
            }

            Filter {
                id: extensionFilter
                anchors.top: sizeParams.bottom
                labelText: "Extension"
                inputPlaceholder: "[jpeg|png|rar|...]"
                onAddClicked: {
                    dataModel.addExtensionFilter(inputText)
                }
            }

            Filter {
                id: md5Filter

                property var pattern: /^[a-f0-9]{32}$/
                anchors.top: extensionFilter.bottom
                labelText: "Md5 hash"
                inputPlaceholder: "md5"
                validator: RegExpValidator { regExp: md5Filter.pattern }
                validatorPass: {
                    md5Filter.pattern.test(inputText)
                }
                onAddClicked: {
                    dataModel.addHashFilter(inputText)
                }
            }

            Filter {
                id: nameFilter
                anchors.top: md5Filter.bottom
                labelText: "File name"
                inputPlaceholder: "Any valid regex"
                onAddClicked: {
                    dataModel.addNameFilter(inputText)
                }
            }

            ListView {
                id: filtersList

                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: nameFilter.bottom
                anchors.bottom: newSearchButton.top

                clip: true
                model: dataModel.filtersModel

                delegate: BorderImage {
                    id: item

                    width: parent.width
                    height: 30
                    source: "../images/delegate.png"
                    border.top: 5; border.bottom: 5
                    border.left: 5; border.right: 5

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: removeButton.enabled = true
                        onExited: removeButton.enabled = false
                    }

                    Text {
                        id: paramsLabel
                        font.pixelSize: 16
                        color: "#333"

                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        elide: Text.ElideRight
                        text: display
                    }

                    Image {
                        id: removeButton

                        height: parent.height
                        width: removeButton.height
                        source: "../images/remove_button.png"
                        anchors.margins: 5
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        enabled: false
                        opacity: enabled ? 1 : 0.5
                        MouseArea {
                            id: removeMouseArea
                            anchors.fill: parent
                            onClicked: {
                                dataModel.filtersModel.removeFilter(index)
                            }
                        }
                    }
                }
            }

            Button {
                id: newSearchButton
                anchors.margins: 5
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                text: "New Search"
                onClicked: {
                    dataModel.search()
                }
            }
        }

        Rectangle {
            id: searchResult

            Layout.fillWidth: true
            Layout.fillHeight: true
            border.color: "gray"
            radius: 5

            TableView {
                id: resultsTable

                anchors.margins: 5
                anchors.fill: parent

                TableViewColumn {
                    role: "id"
                    title: "Id"
                    visible: false
                }

                TableViewColumn {
                    role: "name"
                    title: "Name"
                    width: parent.width
                }

                model: dataModel

                onCurrentRowChanged: {
                    mainWindow.fileInfoDbId = dataModel.getFileId(resultsTable.currentRow);
                }
            }
        }
    }
}
