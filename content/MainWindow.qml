import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import QtQml.Models 2.2

ApplicationWindow {
    id: mainWindow
    width: Screen.width
    height: Screen.height
    minimumWidth: 400
    minimumHeight: 300
    visible: true

    onClosing: {
        welcomeWindow.close();
    }

    property int fileInfoDbId: -1

    RowLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Rectangle {
            id: tools
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumWidth: maximumWidth
            border.color: "black"
            radius: 5

            property int minimumWidth: 150
            property int maximumWidth: Math.min(mainLayout.width * 0.25, 200)

            ResizeHorizontal {
                id: resizeTools
                anchors.right: parent.right
                onPositionChanged: {
                    var diff = resizeTools.difference
                    if (tools.width + diff >= tools.minimumWidth &&
                        tools.width + diff <= tools.maximumWidth) {

                        tools.width += diff

                        toolView.width -= diff
                        toolView.x += diff

                        fileInfo.width -= diff
                        fileInfo.x += diff
                    }
                }
            }

            ListView {
                id: toolsList
                spacing: 5
                anchors.fill: parent
                anchors.margins: 5
                model: toolModel

                delegate: Rectangle {
                    width: parent.width
                    height: 50
                    radius: 5
                    border.color: "black"

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        onClicked: {
                            var component = Qt.createComponent(view);
                            var tab = tabView.addTab(name, component);
                            tabView.currentIndex = tabView.count - 1;
                            tab.active = true;
                            tab.item.dataModel = model;
                        }
                    }

                    Image {
                        id: toolImage
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                        anchors.verticalCenter: parent.verticalCenter
                        height: parent.height * 0.7
                        width: height
                        fillMode: Image.PreserveAspectFit
                        source: image
                    }

                    Text {
                        id: toolName

                        text: name
                        color: "#333"
                        font.pixelSize: 22

                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: toolImage.right
                        anchors.right: parent.right
                        anchors.leftMargin: 5
                        anchors.rightMargin: 15
                    }
                }
            }
        }

        ColumnLayout {
            id: views
            spacing: 10

            Rectangle {
                id: toolView
                Layout.fillHeight: true
                Layout.fillWidth: true
                border.color: "black"
                radius: 5

                property int minimumHeight: 0.5 * mainLayout.height

                ResizeVertical {
                    id: resizeToolView
                    anchors.bottom: parent.bottom
                    onPositionChanged: {
                        var diff = resizeToolView.difference
                        if (toolView.height + diff >= toolView.minimumHeight &&
                            fileInfo.height - diff >= fileInfo.minimumHeight) {

                            toolView.height += resizeToolView.difference

                            fileInfo.height -= resizeToolView.difference
                            fileInfo.y += resizeToolView.difference
                        }
                    }
                }

                TabView {
                    id: tabView
                    anchors.top: parent.top
                    anchors.fill: parent
                    anchors.margins: 5
                    tabPosition: Qt.TopEdge
                    style: TabViewStyle {
                        frameOverlap: 1
                        tab: Rectangle {
                            color: styleData.selected ? "black" : "white"
                            border.color: "black"
                            implicitWidth: Math.max(text.width + closeButton.width + 4, 120)
                            implicitHeight: 20
                            radius: 1
                            Text {
                                id: text
                                anchors.centerIn: parent
                                text: styleData.title
                                color: styleData.selected ? "white" : "black"
                            }
                            Button {
                                id: closeButton
                                anchors.right: parent.right
                                anchors.rightMargin: 4
                                anchors.verticalCenter: parent.verticalCenter
                                height: 12
                                style: ButtonStyle {
                                    background: Rectangle {
                                        implicitWidth: 12
                                        implicitHeight: 12
                                        radius: width / 2
                                        color: control.hovered ? "#eee" : "#ccc"
                                        border.color: "lightgray"
                                        Text {
                                            text: "x"
                                            anchors.centerIn: parent
                                            color: "black"
                                        }
                                    }
                                }
                                onClicked: tabView.removeTab(styleData.index)
                            }
                        }
                        frame: Rectangle { color: "transparent" }
                        tabsAlignment: Qt.AlignHCenter
                        tabsMovable: true
                    }
                }
            }

            Rectangle {
                id: fileInfo
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: 0.3 * mainLayout.height
                border.color: "black"
                radius: 5

                property int minimumHeight: 0.2 * mainLayout.height

                ResizeVertical {
                    id: resizeFileInfo
                    anchors.top: parent.top
                    onPositionChanged: {
                        var diff = resizeFileInfo.difference
                        if (toolView.height + diff >= toolView.minimumHeight &&
                            fileInfo.height - diff >= fileInfo.minimumHeight) {

                            toolView.height += diff

                            fileInfo.height -= diff
                            fileInfo.y += diff
                        }
                    }
                }

                FileInfoView {
                    anchors.fill: parent
                    fileId: fileInfoDbId
                }
            }
        }
    }
}
