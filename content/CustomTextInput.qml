import QtQuick 2.4

BorderImage {
    id: customTextInput

    signal accepted

    property alias validator: textInput.validator
    property string text: textInput.text
    property alias placeholder: placeholderText.text

    width: 100
    height: 50
    anchors.margins: 10
    anchors.verticalCenter: parent.verticalCenter
    source: "../images/textfield.png"
    border.top: 4; border.bottom: 4
    border.left: 8; border.right: 8

    TextInput {
        id: textInput
        clip: true
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        font.pixelSize: 12
        Text {
            id: placeholderText
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            visible: !(parent.text.length || parent.inputMethodComposing)
            font: parent.font
            color: "#aaa"
        }
    }

    onAccepted: {
        textInput.text = ""
    }
}

