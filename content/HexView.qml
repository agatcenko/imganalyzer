import QtQuick 2.4
import QtQuick.Layouts 1.1

Item {
    id: root
    property variant hexModel
    property int visibleLines: height / 30
    property int bytesInLine: 16
    property int lineHeight: (height - height / visibleLines) / visibleLines
    property int cellWidth: width / 28
    property int hexWidth: cellWidth * 16 / bytesInLine
    property int addressWidth: cellWidth * 4
    property int asciiWidth: (width - addressWidth) / bytesInLine - hexWidth

    Connections {
        target: hexModel
        onAddressChanged: {
            hexViewer.currentIndex = (address - hexModel.rowCount() * bytesInLine * (FileContent.currentPage - 1)) / bytesInLine
            hexViewer.positionViewAtIndex(hexViewer.currentIndex, ListView.Contain)
        }
    }

    Component {
        id: drawLine
        ListView {
            id: line
            height: lineHeight
            width: hexViewer.width
            orientation: ListView.Horizontal
            interactive: false

            model: lineData
            delegate: Rectangle {
                property bool addressField: index === 0
                property int viewOffset: index > bytesInLine ? index - bytesInLine : index
                property bool selected: (viewOffset === hexModel.offset) && (lineData[0] === hexModel.address)

                color: addressField ? "#AAA" : !selected && index < bytesInLine + 1 ? "#F7F7F7" : !selected ? "#AAA" : "#CCC"
                width: addressField ? addressWidth : index < bytesInLine + 1 ? hexWidth : asciiWidth
                height: lineHeight

                Text {
                    anchors.centerIn: parent
                    text: lineData[index]
                    font.pixelSize: Math.min(parent.height / 2, parent.width / 2)
                    color: selected ? "gray" : "black"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked : {
                        hexModel.address = lineData[0]
                        hexModel.offset = index
                        hexViewer.forceActiveFocus()
                    }
                }
            }
        }
    }

    Column {
        anchors.fill: parent
        spacing: 2
        anchors.topMargin: 5

        PagesControl {
            id: pagesControl
            customHeight: 30
            displayCodecs: false
        }

        ListView {
            id: hexViewer
            width: parent.width
            height: parent.height - pagesControl.customHeight
            model: hexModel
            clip: true
            delegate: drawLine
            maximumFlickVelocity: 2 * height

            focus: true
            Keys.onPressed: {
                hexModel.pressKey(event.key)
            }
        }
    }
}
