import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

Item {
    id: root

    property variant dataModel;

    TableView {
        id: tableView

        anchors.fill: parent
        anchors.margins: 10

        TableViewColumn {
            role: "id"
            title: "Id"
            visible: false
        }

        TableViewColumn {
            id: timeColumn
            role: "time"
            title: "Time"
            width: 180
        }
        TableViewColumn {
            id: nameColumn
            role: "file"
            title: "File Name"
            width: parent.width - timeColumn.width - eventColumn.width
        }
        TableViewColumn {
            id: eventColumn
            role: "event"
            title: "Event"
            width: 200
        }

        model: dataModel

        onCurrentRowChanged: {
            mainWindow.fileInfoDbId = dataModel.getFileId(tableView.currentRow);
        }
    }
}
