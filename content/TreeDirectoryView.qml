import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

Item {
    id: treeDirectoryView

    anchors.topMargin: 10
    anchors.fill: parent

    property variant dataModel;

    ItemSelectionModel {
        id: sel
        model: dataModel
    }

    TreeView {
        id: treeView
        anchors.left: parent.left
        anchors.right: tableView.left
        height: parent.height
        anchors.margins: 5
        model: dataModel
        selection: sel

        style: TreeViewStyle {
            itemDelegate: Item {
                Image {
                    id: itemImage
                    width: 20
                    height: parent.height
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    source: styleData.value["image"]
                }
                Text {
                    id: itemName
                    anchors.left: itemImage.right
                    anchors.leftMargin: 10
                    text: styleData.value["name"]
                }
            }
        }

        onCurrentIndexChanged: {
            console.log("current index", currentIndex)
            mainWindow.fileInfoDbId = dataModel.getFileId(currentIndex)
        }

        TableViewColumn {
            title: "Name"
            role: "name"
            resizable: true
        }

        onDoubleClicked: isExpanded(index) ? collapse(index) : expand(index)
    }

    TableView {
        id: tableView

        height: parent.height
        width: 400
        anchors.right: parent.right
        anchors.margins: 5

        TableViewColumn {
            role: "name"
            title: "Children"
        }

        style: TableViewStyle {
            itemDelegate: Item {
                Image {
                    id: tableItemImage
                    height: parent.height - 2
                    width: tableItemImage.height
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    source: styleData.value["image"]
                }

                Text {
                    anchors.left: tableItemImage.right
                    anchors.leftMargin: 10
                    text: styleData.value["name"]
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
            }
        }

        model: dataModel.childrenModel
        onCurrentRowChanged: {
            mainWindow.fileInfoDbId = dataModel.childrenModel.getFileId(currentRow)
        }
    }
}
