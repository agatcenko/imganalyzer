import QtQuick 2.4

Rectangle {
    id: root

    width: 1
    height: parent.height
    color: "transparent"

    signal positionChanged
    property int difference: mouseArea.mouseX - mouseArea.oldMouseX

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        property int oldMouseX
        hoverEnabled: true

        cursorShape: "SizeHorCursor"

        onPressed: {
            oldMouseX = mouseX
        }

        onPositionChanged: {
            if (pressed) {
                root.positionChanged()
            }
        }
    }
}
