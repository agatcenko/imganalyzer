import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Item {
    width: parent.width
    height: parent.height
    property int margin: 15
    property string nextPage: "../content/NewCaseNext.qml"

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: margin

        InputField {
            id: examinerField
            text: "Examiner:"
        }

        InputField {
            id: caseNumberField
            text: "Case Number:"
        }
    }
}
