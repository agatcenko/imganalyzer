import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4

Item {
    id: root

    property int currentFileId

    onCurrentFileIdChanged: {
        FileExtract.changeExtractiveFile(currentFileId)
    }

    Column {
        spacing: 5
        anchors.fill: parent

        RowLayout {
            id: outputSettings
            width: parent.width
            height: 30

            TextField {
                id: outputDir
                Layout.fillWidth: true
                placeholderText: qsTr("Choose directory to extract files")
            }

            Button {
                id: browseButton
                text: "Browse"

                onClicked: {
                    fileDialog.open()
                }
            }
        }

        RowLayout {
            id: extractionInfo
            width: parent.width
            height: 30

            Text {
                Layout.fillWidth: true
                text: FileExtract.extractionInfo
            }

            Button {
                id: extractButton
                text: "Extract"

                onClicked: {
                    if (outputDir.text !== "") {
                        FileExtract.extractFiles(outputDir.text);
                    }
                }
            }
        }

        TextArea {
            width: parent.width
            height: parent.height - (outputSettings.height + extractionInfo.height + 10)

            readOnly: true
            text: FileExtract.extractionProgress
        }
    }

    FileDialog {
        id: fileDialog
        selectFolder: true
        title: "Choose output directory"
        onAccepted: {
            var path = fileDialog.fileUrl.toString();
            path = path.replace(/^(file:\/{2})|(qrc:\/{2})|(http:\/{2})/,"");
            var cleanPath = decodeURIComponent(path);
            outputDir.text = cleanPath;
        }
    }
}
