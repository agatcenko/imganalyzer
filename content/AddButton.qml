import QtQuick 2.4

Item {
    id: addButton

    signal clicked

    height: parent.height
    width: addButton.height
    anchors.verticalCenter: parent.verticalCenter
    Image {
        anchors.fill: parent
        opacity: enabled ? 1 : 0.5
        source: "../images/add_button.png"
    }
    MouseArea {
        id: addMouseArea
        anchors.fill: parent
        onClicked: {
            addButton.clicked()
        }
    }
}
