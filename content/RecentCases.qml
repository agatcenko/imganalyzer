import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Item {
    width: parent.width
    height: parent.height
    property string nextPage: ""

    Column {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 5

        TableView {
            id: casesView

            width: parent.width
            height: parent.height - buttons.height

            TableViewColumn {
                role: "id"
                title: "ID"
                visible: false
            }

            TableViewColumn {
                id: nameColumn
                role: "name"
                title: "Case Name"
                width: parent.width / 3
            }

            TableViewColumn {
                id: pathColumn
                role: "path"
                title: "Image Path"
                width: parent.width - nameColumn.width
            }

            model: historyModel
        }

        RowLayout {
            id: buttons

            width: parent.width
            height: 50

            StylishButton {
                text: "Delete"
                onClicked: {
                    if (casesView.currentRow >= 0) {
                        historyModel.removeCase(casesView.currentRow)
                    }
                }
            }

            Item {
                Layout.fillWidth: true
            }

            StylishButton {
                text: "Open"
                onClicked: {
                    if (casesView.currentRow >= 0) {
                        var db_name = historyModel.getCaseName(casesView.currentRow);
                        toolsBuilder.connect(db_name);
                        var component = Qt.createComponent("MainWindow.qml");
                        var window = component.createObject(root);
                        window.show();
                        welcomeWindow.width = 0;
                        welcomeWindow.height = 0;
                    }
                }
            }
        }
    }
}
