import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
    id: lastCaseWindow
    width: parent.width
    height: parent.height
    property string nextPage: ""

    Component.onCompleted: {
        if (lastCase.readLastCaseConfig() !== 0) {
            status.text = "Last case didn`t found!"
            openButton.visible = false;
        }
        else {
            status.text = "Last case: " + lastCase.dbName;
            openButton.visible = true;
        }
    }

    Text {
        id: status
        text: "Last Case"

        font.pixelSize: 32
        color: "white"

        anchors.top: parent.top
        anchors.topMargin: 70
        anchors.horizontalCenter: parent.horizontalCenter
    }

    StylishButton {
        id: openButton
        text: "Open Case"

        anchors.top: status.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter

        onClicked: {
            toolsBuilder.connect();
            var component = Qt.createComponent("MainWindow.qml");
            var window = component.createObject(root);
            window.show();
            welcomeWindow.width = 0;
            welcomeWindow.height = 0;
        }
    }
}
