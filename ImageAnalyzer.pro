TEMPLATE = app

QT += qml quick widgets

QMAKE_CXXFLAGS += -std=c++11

include(src/src.pri)

RESOURCES += qml.qrc

OTHER_FILES += config/data_base.yml \
               config/tools.yml

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
