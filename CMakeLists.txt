cmake_minimum_required(VERSION 2.8.3)

add_definitions(--std=c++11 -Wall)

include(FindPkgConfig)
pkg_check_modules(YamlCpp yaml-cpp)

include_directories(${YamlCpp_INCLUDE_DIRS})
link_directories(${YamlCpp_LIBRARY_DIRS})

project(image_analyzer)
file(GLOB_RECURSE SOURCES "src/*.cpp")

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_PREFIX_PATH "/opt/Qt/5.5/gcc_64/lib/cmake/Qt5/")
find_package(Qt5 COMPONENTS Quick Qml Widgets)
qt5_add_resources(RESOURCES qml.qrc)

add_executable(${PROJECT_NAME} ${SOURCES} ${RESOURCES})

target_link_libraries(${PROJECT_NAME} 
    tsk 
    pqxx 
    pq
    ${YamlCpp_LIBRARIES}
    Qt5::Qml
    Qt5::Quick
    Qt5::Widgets
)

file(GLOB_RECURSE CONFIGS "config/*.yml")
install(FILES ${CONFIGS} DESTINATION ${PROJECT_BINARY_DIR}/config)
