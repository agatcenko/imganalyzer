#pragma once

#include "content_reader.h"
#include "../data_base/db_types.h"
#include "../data_base/db_parser.h"

#include <map>
#include <vector>
#include <QObject>

struct ExtractionInfo
{
    int fileCount = 0;
    QString fileType = "Undefined";
    unsigned long long contentSize = 0;
};

struct ExtractedItem
{
    long id;
    TSK_FS_META_TYPE_ENUM type;
    QString name;

    ExtractedItem(long fileId, TSK_FS_META_TYPE_ENUM fileType, QString fileName) :
        id(fileId), type(fileType), name(fileName) {}
};

class ContentSaver : public QObject
{
    Q_OBJECT

public:
    ContentSaver(DBParser *dbParser, QObject *parent = 0);
    virtual ~ContentSaver();

    void setId(int id);
    bool isBusy() const;
    ExtractionInfo getExtractionInfo();
    void extractFiles(QString outputDir);

signals:
    void fileExtracted(QString name);
    void extractionEnd();

private:
    int _fileId = -1;
    bool _busy = false;
    const int BUFFER_SIZE = 1048576; // 1Mb

    DBParser* _dbParser;
    ContentReader* _contentReader;
    std::map<long, std::vector<ExtractedItem> > _extractedTree;

    void getChildrenInfo(ExtractionInfo &info, long id);
    void extractFile(ExtractedItem item, QString outputDir);
};
