#include "content_storage.h"

#include <math.h>

ContentStorage::ContentStorage(DBParser* dbParser, QObject *parent) :
    _dbParser(dbParser), QObject(parent)
{
    _currentPage = 1;
    _totalPages = 0;
    _contentBuffer.fill(0);
    _contentReader = new ContentReader(_dbParser);
}

ContentStorage::~ContentStorage()
{
    delete _contentReader;
}

QByteArray ContentStorage::getContentString() const
{
    QByteArray res;
    for (const auto byte : _contentBuffer) {
        res += byte;
    }

    return res;
}

char ContentStorage::getContentAt(int i) const
{
    if (i >= 0 && i < PAGE_SIZE) {
        return _contentBuffer[i];
    }

    return char();
}

int ContentStorage::readFileContent(int id, int pageNumber)
{
    if (id != _currentId) {
        _currentId = id;
        int64_t fileSize;
        _dbParser->getFileSize(fileSize, id);
        _totalPages = (int)ceil((fileSize + 0.0) / PAGE_SIZE);
        emit totalPagesChanged();
    }

    _currentPage = pageNumber;
    emit currentPageChanged();

    int res = _contentReader->readFile(id, (pageNumber - 1) * PAGE_SIZE,
                             PAGE_SIZE, _contentBuffer.data());

    if (res == -1) {
        setreadStatusText("Read Error");
        setreadStatusColor("red");
    }
    else {
        setreadStatusText("Read OK");
        setreadStatusColor("green");
        emit contentChanged();
    }

    return res;
}

void ContentStorage::nextPage()
{
    if (_currentPage < _totalPages) {
        readFileContent(_currentId, ++_currentPage);
    }
}

void ContentStorage::previousPage()
{
    if (_currentPage > 1) {
        readFileContent(_currentId, --_currentPage);
    }
}
