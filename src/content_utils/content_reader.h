#pragma once

#include "../data_base/db_parser.h"

#include <map>
#include <queue>
#include <utility>
#include <tsk/libtsk.h>

class ContentReader
{
public:
    ContentReader(DBParser* dbParser);
    ~ContentReader();

    int openImage();
    void closeImage();
    int openFile(const int id);
    int readFile(const int id, TSK_OFF_T offset, size_t length, char* buffer);

private:
    DBParser* _dbParser;
    TSK_IMG_INFO* _imgInfo = nullptr;

    std::map<TSK_OFF_T, TSK_FS_INFO*> _openedFs;

    const size_t _openedFilesMaxSize = 10;
    std::queue<int> _openedFilesId;
    std::map<int, std::pair<TSK_FS_FILE*, const TSK_FS_ATTR*> > _openFiles;
};
