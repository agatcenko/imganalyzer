#include "content_saver.h"

#include <vector>
#include <QDir>
#include <QFile>
#include <QDataStream>

using namespace std;

ContentSaver::ContentSaver(DBParser *dbParser, QObject *parent) :
    _dbParser(dbParser), QObject(parent)
{
    _contentReader = new ContentReader(_dbParser);
}

ContentSaver::~ContentSaver()
{
    delete _contentReader;
}

void ContentSaver::setId(int id)
{
    _fileId = id;
    _extractedTree.clear();
}

bool ContentSaver::isBusy() const
{
    return _busy;
}

ExtractionInfo ContentSaver::getExtractionInfo()
{
    ExtractionInfo info;

    FileInfo fileInfo;
    _dbParser->getFileInfo(fileInfo, _fileId);
    info.fileType = QString::fromStdString(metaTypeEnumToString(fileInfo.type));
    info.fileCount = 1;
    info.contentSize = fileInfo.size;
    getChildrenInfo(info, _fileId);

    return info;
}

void ContentSaver::extractFiles(QString outputDir)
{
    _busy = true;

    FileInfo fileInfo;
    _dbParser->getFileInfo(fileInfo, _fileId);
    extractFile(ExtractedItem(_fileId, fileInfo.type,
                              QString::fromStdString(fileInfo.name)), outputDir);

    emit extractionEnd();

    _busy = false;
}

void ContentSaver::extractFile(ExtractedItem item, QString outputDir)
{
    QString fullPath = outputDir + "/" + item.name;

    if (item.type == TSK_FS_META_TYPE_DIR) {
        QDir directory(outputDir);
        directory.mkdir(item.name);
    }
    else {
        QFile file(fullPath);
        if (!file.open(QIODevice::WriteOnly)) {
            return;
        }
        QDataStream out(&file);

        int offset = 0;
        char buffer[BUFFER_SIZE];
        int len = _contentReader->readFile(item.id, offset, BUFFER_SIZE, buffer);
        while (len > 0) {
            out.writeRawData(buffer, len);
            offset += len;
            len = _contentReader->readFile(item.id, offset, BUFFER_SIZE, buffer);
        }
        file.close();
    }

    emit fileExtracted(item.name);

    if (_extractedTree.count(item.id)) {
        vector<ExtractedItem> children = _extractedTree[item.id];
        for (auto child : children) {
            extractFile(child, fullPath);
        }
    }
}

void ContentSaver::getChildrenInfo(ExtractionInfo &info, long id)
{
    vector<pair<long, DB_OBJECT_TYPE> > children;
    if (_dbParser->getChildrenIds(id, children)) {
        return;
    }

    if (!children.size()) {
        return;
    }

    vector<ExtractedItem> extractedItems;
    for (size_t i = 0; i < children.size(); ++i) {
        FileInfo fileInfo;
        _dbParser->getFileInfo(fileInfo, children[i].first);
        info.fileCount++;
        info.contentSize += fileInfo.size;
        getChildrenInfo(info, children[i].first);

        extractedItems.emplace_back(children[i].first, fileInfo.type,
                                    QString::fromStdString(fileInfo.name));
    }

    _extractedTree[id] = extractedItems;
}
