#include "content_reader.h"
#include "../data_base/db_types.h"

#include <iostream>

using namespace std;

ContentReader::ContentReader(DBParser* dbParser) :
    _dbParser(dbParser)
{

}

ContentReader::~ContentReader()
{
    closeImage();
}

int ContentReader::openImage()
{
    if (_imgInfo != nullptr) {
        closeImage();
    }

    ImageInfo info;
    _dbParser->getImageInfo(info, 1);

    TSK_TCHAR** img_pathes = new TSK_TCHAR*[1];
    img_pathes[0] = (TSK_TCHAR *)(info.path.c_str());
    _imgInfo = tsk_img_open_utf8(1, img_pathes, TSK_IMG_TYPE_DETECT, 0);

    delete[] img_pathes;

    if (_imgInfo == NULL) {
        return -1;
    }

    return 0;
}

void ContentReader::closeImage()
{
    if (_imgInfo != nullptr) {
        _imgInfo = nullptr;
        tsk_img_close(_imgInfo);
    }

    for (auto fileSystem : _openedFs) {
        tsk_fs_close(fileSystem.second);
    }

    for (auto file : _openFiles) {
        tsk_fs_file_close(file.second.first);
    }

    while (!_openedFilesId.empty()) {
        _openedFilesId.pop();
    }
    _openFiles.clear();
}

int ContentReader::readFile(const int id, TSK_OFF_T offset,
                           size_t length, char* buffer)
{
    if (!_openFiles.count(id)) {
        if (openFile(id) != 0) {
            cout << "Error while read file with id: " << id << endl;
            return -1;
        }
    }

    if (length == 0) {
        return 0;
    }

    TSK_FS_FILE* fsFile = _openFiles[id].first;
    const TSK_FS_ATTR* fsAttr = _openFiles[id].second;

    if (fsAttr == NULL || offset >= fsAttr->size) {
        cout << "Nothing to read" << endl;
        return 0;
    }

    int bytesRead = tsk_fs_attr_read(fsAttr, offset, buffer, length, TSK_FS_FILE_READ_FLAG_NONE);

    if (bytesRead == -1) {
        cout << "Error reading file with id: " << id <<
                " and metaAddr: " << fsFile->meta->addr << endl;
    }

    return bytesRead;
}

int ContentReader::openFile(const int id)
{
    if (_imgInfo == nullptr) {
        if (openImage() != 0) {
            return -1;
        }
    }

    FileLocationInfo location;
    if (_dbParser->getFileLocationInFs(location, id) != 0) {
        cout << "Error while open file with id: " << id << endl;
        return -1;
    }

    TSK_FS_INFO* fsInfo = _openedFs[location.fsOffset];

    if (fsInfo == NULL) {
        fsInfo = tsk_fs_open_img(_imgInfo, location.fsOffset, TSK_FS_TYPE_DETECT);
        if (fsInfo == NULL) {
            cout << "Error while open file system with offset: " << location.fsOffset << endl;
            return -1;
        }
        _openedFs[location.fsOffset] = fsInfo;
    }

    TSK_FS_FILE* fsFile = tsk_fs_file_open_meta(fsInfo, NULL, location.metaAddr);
    if (fsFile == NULL) {
        cout << "Error while open file meta" << endl;
        return -1;
    }

    const TSK_FS_ATTR* fsAttr = tsk_fs_file_attr_get_id(fsFile, location.attrId);
    if (location.attrType != TSK_FS_ATTR_TYPE_NOT_FOUND && fsAttr == NULL) {
        cout << "Error while get file attribute" << endl;
        return -1;
    }

    if (_openedFilesId.size() == _openedFilesMaxSize) {
        int oldestFileId = _openedFilesId.front();
        _openedFilesId.pop();
        if (_openFiles.count(oldestFileId)) {
            _openFiles.erase(oldestFileId);
        }
    }

    _openFiles[id] = make_pair(fsFile, fsAttr);

    return 0;
}
