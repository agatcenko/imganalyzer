#pragma once

#include "../qml_macro.h"
#include "content_reader.h"
#include "../data_base/db_parser.h"

#include <array>

class ContentStorage : public QObject
{
    Q_OBJECT

public:
    ContentStorage(DBParser *dbParser, QObject* parent = 0);
    virtual ~ContentStorage();

    static const uint32_t PAGE_SIZE = 1600;

    char getContentAt(int i) const;
    QByteArray getContentString() const;

    Q_INVOKABLE int readFileContent(int id, int pageNumber);
    Q_INVOKABLE void nextPage();
    Q_INVOKABLE void previousPage();

signals:
    void contentChanged();
    void currentPageChanged();
    void totalPagesChanged();
    void readStatusTextChanged();
    void readStatusColorChanged();

private:
    DBParser* _dbParser;
    ContentReader* _contentReader;
    std::array<char, PAGE_SIZE> _contentBuffer;

    int _currentId = -1;

    REGISTER_QML_VALUE(int, currentPage)
    REGISTER_QML_VALUE(int, totalPages)
    REGISTER_QML_VALUE(QString, readStatusText)
    REGISTER_QML_VALUE(QString, readStatusColor)
};
