HEADERS += \
    $$PWD/data_base/data_prepare.h \
    $$PWD/data_base/db_creator.h \
    $$PWD/data_base/db_parser.h \
    $$PWD/data_base/db_types.h \
    $$PWD/data_base/query_utils.h \
    $$PWD/content_utils/content_reader.h \
    $$PWD/content_utils/content_storage.h \
    $$PWD/content_utils/content_saver.h \
    $$PWD/image_analyzer.h \
    $$PWD/config_utils/yaml_reader.h \
    $$PWD/config_utils/config_utils.h \
    $$PWD/views/welcome_window/newcase.h \
    $$PWD/views/welcome_window/lastcase.h \
    $$PWD/views/welcome_window/history_model.h \
    $$PWD/views/tree/tree_item.h \
    $$PWD/views/tree/tree_model.h \
    $$PWD/views/tree/children_table_model.h \
    $$PWD/views/search/filters.h \
    $$PWD/views/search/search_model.h \
    $$PWD/views/timeline/timeline_model.h \
    $$PWD/views/file_info/file_hex_view.h \
    $$PWD/views/file_info/file_info_view.h \
    $$PWD/views/file_info/file_string_view.h \
    $$PWD/views/file_info/file_extract_view.h \
    $$PWD/tools/tool.h \
    $$PWD/tools/tools_builder.h \
    $$PWD/qml_macro.h

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/data_base/data_prepare.cpp \
    $$PWD/data_base/db_creator.cpp \
    $$PWD/data_base/db_parser.cpp \
    $$PWD/data_base/db_types.cpp \
    $$PWD/content_utils/content_reader.cpp \
    $$PWD/content_utils/content_storage.cpp \
    $$PWD/content_utils/content_saver.cpp \
    $$PWD/image_analyzer.cpp \
    $$PWD/config_utils/yaml_reader.cpp \
    $$PWD/config_utils/config_utils.cpp \
    $$PWD/views/welcome_window/newcase.cpp \
    $$PWD/views/welcome_window/lastcase.cpp \
    $$PWD/views/welcome_window/history_model.cpp \
    $$PWD/views/tree/tree_item.cpp \
    $$PWD/views/tree/tree_model.cpp \
    $$PWD/views/tree/children_table_model.cpp \
    $$PWD/views/search/filters.cpp \
    $$PWD/views/search/search_model.cpp \
    $$PWD/views/timeline/timeline_model.cpp \
    $$PWD/views/file_info/file_hex_view.cpp \
    $$PWD/views/file_info/file_info_view.cpp \
    $$PWD/views/file_info/file_string_view.cpp \
    $$PWD/views/file_info/file_extract_view.cpp \
    $$PWD/tools/tool.cpp \
    $$PWD/tools/tools_builder.cpp

LIBS += -lyaml-cpp
LIBS += -lpqxx
LIBS += -lpq
LIBS += -ltsk
