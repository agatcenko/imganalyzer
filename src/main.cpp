#include "tools/tool.h"
#include "tools/tools_builder.h"
#include "views/tree/tree_model.h"
#include "views/welcome_window/newcase.h"
#include "views/welcome_window/lastcase.h"
#include "views/welcome_window/history_model.h"

#include <QtQml>
#include <QApplication>
#include <QQmlApplicationEngine>

#include <memory>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<NewCase>("CppCases", 1, 0, "NewCase");
    QQmlApplicationEngine engine;

    std::shared_ptr<LastCase> lastCase(new LastCase(&engine));
    engine.rootContext()->setContextProperty("lastCase", lastCase.get());

    std::shared_ptr<HistoryModel> historyModel(new HistoryModel());
    engine.rootContext()->setContextProperty("historyModel", historyModel.get());

    std::shared_ptr<ToolsBuilder> toolsBuilder(new ToolsBuilder(&engine));
    engine.rootContext()->setContextProperty("toolsBuilder", QVariant::fromValue(toolsBuilder.get()));

    engine.load(QUrl(QStringLiteral("qrc:/content/main.qml")));

    return app.exec();
}
