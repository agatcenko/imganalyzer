#pragma once

#include "../config_utils/yaml_reader.h"
#include "../content_utils/content_storage.h"
#include "../data_base/db_parser.h"

#include <memory>
#include <QObject>
#include <QString>
#include <QAbstractItemModel>
#include <QQmlApplicationEngine>

class ToolsBuilder : public QObject
{
    Q_OBJECT
public:
    ToolsBuilder(QQmlApplicationEngine* engine, QObject *parent = 0);
    ~ToolsBuilder();

    QList<QObject*> getToolsList() const;
    QMap<QString, QObject*> getContainersList() const;

    Q_INVOKABLE void connect(QString name = "");

    template <class ToolModel>
    void registerModel(QString name)
    {
        if (_toolModels.count(name))
            return;

        _toolModels[name] = std::make_shared<ToolModel>(_dbParser, _engine);
    }

    template <class T>
    void registerContainer(QString name)
    {
        _containers[name] = new T(_dbParser);
    }

private:
    YamlReader _configReader;
    const QString _configTools = "config/tools.yml";

    DBParser* _dbParser = nullptr;

    QQmlApplicationEngine* _engine;
    QMap<QString, QObject*> _containers;
    QMap<QString, std::shared_ptr<QAbstractItemModel> > _toolModels;
    QList<QObject*> _toolsList;
    ContentStorage* _contentStorage = nullptr;

    int connectToExistDB(QString name);
    void readToolsParams();
};
