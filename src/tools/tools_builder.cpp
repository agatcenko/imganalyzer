#include "tool.h"
#include "tools_builder.h"
#include "../config_utils/config_utils.h"
#include "../views/tree/tree_model.h"
#include "../views/search/search_model.h"
#include "../views/timeline/timeline_model.h"
#include "../views/file_info/file_hex_view.h"
#include "../views/file_info/file_info_view.h"
#include "../views/file_info/file_string_view.h"
#include "../views/file_info/file_extract_view.h"

#include <string>
#include <iostream>
#include <QtQml>

ToolsBuilder::ToolsBuilder(QQmlApplicationEngine *engine, QObject *parent) :
     _engine(engine), QObject(parent)
{

}

ToolsBuilder::~ToolsBuilder()
{
    if (_dbParser != nullptr) {
        delete _dbParser;
    }

    if (_contentStorage != nullptr) {
        delete _contentStorage;
    }

    if (_containers.size()) {
        for (auto i = _containers.begin(); i != _containers.end(); ++i) {
            //delete i.value();
        }
    }
}

void ToolsBuilder::connect(QString name)
{
    if (connectToExistDB(name) == 0) {
        registerModel<TreeModel>("Tree");
        registerModel<TimeLineModel>("TimeLine");
        registerModel<SearchModel>("Search");

        _contentStorage = new ContentStorage(_dbParser);

        registerContainer<FileInfoView>("FileInfo");

        _containers["FileHex"] = new FileHexView(_contentStorage);
        _containers["FileString"] = new FileStringView(_contentStorage);
        _containers["FileExtract"] = new FileExtractView(_dbParser);
        _containers["FileContent"] = _contentStorage;

        QObject::connect(_contentStorage, SIGNAL(contentChanged()),
                         _containers["FileHex"], SLOT(onContentChanged()));
        QObject::connect(_contentStorage, SIGNAL(contentChanged()),
                         _containers["FileString"], SLOT(onContentChanged()));

        readToolsParams();
    }

    _engine->rootContext()->setContextProperty("toolModel", QVariant::fromValue(_toolsList));

    auto containers = getContainersList();
    for (auto i = containers.constBegin(); i != containers.constEnd(); ++i) {
        _engine->rootContext()->setContextProperty(i.key(), QVariant::fromValue(i.value()));
    }
}

QList<QObject*> ToolsBuilder::getToolsList() const
{
    return _toolsList;
}

QMap<QString, QObject *> ToolsBuilder::getContainersList() const
{
    return _containers;
}

int ToolsBuilder::connectToExistDB(QString name)
{
    _dbParser = new DBParser;
    std::string dbName;

    if (name.isEmpty()) {
        dbName = config::getCurrentDataBaseName();
    }
    else {
        dbName = name.toStdString();
    }

    _dbParser->setDBName(dbName);

    return _dbParser->connect();
}

void ToolsBuilder::readToolsParams()
{
    _configReader.setSource(_configTools.toStdString());
    YAML::Node toolsNode;
    _configReader.read(toolsNode, "tools");

    if (toolsNode.IsDefined()) {
        for (size_t i = 0; i < toolsNode.size(); ++i) {
            QString name = QString::fromStdString(toolsNode[i]["name"].as<std::string>());
            if (_toolModels.count(name)) {
                QString image = QString::fromStdString(toolsNode[i]["image"].as<std::string>());
                QString view = QString::fromStdString(toolsNode[i]["view"].as<std::string>());
                _toolsList.append(new Tool(name, image, view, _toolModels[name].get()));
            }
            else {
                std::cout << "No available model for tool " << name.toStdString() << std::endl;
            }
        }
    }
}
