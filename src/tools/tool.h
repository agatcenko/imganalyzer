#pragma once

#include <QString>
#include <QObject>
#include <QAbstractItemModel>

class Tool : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(QString view READ view WRITE setView NOTIFY viewChanged)
    Q_PROPERTY(QAbstractItemModel* model READ model WRITE setModel NOTIFY modelChanged)

public:
    Tool(QObject* parent = 0);
    Tool(QString name, QString image, QString view,
         QAbstractItemModel* model, QObject *parent = 0);
    ~Tool();

    QString name() const;
    void setName(const QString &name);
    QString image() const;
    void setImage(const QString &image);
    QString view() const;
    void setView(const QString &view);
    QAbstractItemModel* model() const;
    void setModel(QAbstractItemModel* model);

signals:
    void nameChanged();
    void imageChanged();
    void viewChanged();
    void modelChanged();

private:
    QString _name;
    QString _image;
    QString _view;
    QAbstractItemModel* _model;
};
