#include "tool.h"

Tool::Tool(QObject* parent) :
    QObject(parent)
{

}

Tool::Tool(QString name, QString image, QString view, QAbstractItemModel *model, QObject *parent) :
    QObject(parent), _name(name), _image(image), _view(view), _model(model)
{

}

Tool::~Tool()
{

}

QString Tool::name() const
{
    return _name;
}

void Tool::setName(const QString &name)
{
    if (name != _name) {
        _name = name;
        emit nameChanged();
    }
}

QString Tool::image() const
{
    return _image;
}

void Tool::setImage(const QString &image)
{
    if (image != _image) {
        _image = image;
        emit imageChanged();
    }
}

QString Tool::view() const
{
    return _view;
}

void Tool::setView(const QString &view)
{
    if (view != _view) {
        _view = view;
        emit viewChanged();
    }
}

QAbstractItemModel* Tool::model() const
{
    return _model;
}

void Tool::setModel(QAbstractItemModel* model)
{
    if (model != _model) {
        _model = model;
        emit modelChanged();
    }
}
