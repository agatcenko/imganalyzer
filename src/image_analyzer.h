#pragma once

#include "data_base/db_creator.h"

#include <tsk/libtsk.h>

#include <map>
#include <string>
#include <fstream>
#include <QFile>
#include <QTextStream>

class ImgAnalyzer : public TskAuto
{
public:
    ImgAnalyzer(DBCreator* db);
    
    virtual ~ImgAnalyzer();
    virtual uint8_t openImageUtf8(int a_numImg, const char* const a_images[], TSK_IMG_TYPE_ENUM a_imgType, unsigned int a_sSize);
    
    virtual TSK_RETVAL_ENUM processFile(TSK_FS_FILE* fs_file, const char* path);
    virtual TSK_RETVAL_ENUM processAttribute(TSK_FS_FILE* fs_file, const TSK_FS_ATTR* fs_attr, const char* path);
    
    virtual TSK_FILTER_ENUM filterFs(TSK_FS_INFO* fs_info);
    virtual TSK_FILTER_ENUM filterVs(const TSK_VS_INFO* vs_info);
    virtual TSK_FILTER_ENUM filterVol(const TSK_VS_PART_INFO* vs_part);
    
    virtual uint8_t handleError();

    int run(int num, const TSK_TCHAR * const images[]);
private:
    DBCreator* _db;

    bool _isVsFound;
    bool _isVolFound;

    int _currentImgId;
    int _currentVsId;
    int _currentVolId;
    int _currentFsId;
    int _objectId;
    
    std::map<std::string, int> pathToId;

    TSK_RETVAL_ENUM saveImgInfo(int a_numImg, const char* const a_images[]);
    TSK_RETVAL_ENUM saveFileInfo(TSK_FS_FILE* fs_file, const TSK_FS_ATTR* fs_attr, const char* path);
    std::string calculateMd5(const TSK_FS_ATTR *fsAttr);
};
