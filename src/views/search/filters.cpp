#include "filters.h"

#include <sstream>

QString TimeFilter::getType() const
{
    return "TimeFilter";
}

QString TimeFilter::getParamsQuery() const
{
    QString timeType = _type == "Changed" ? "ctime"  :
                       _type == "Created" ? "crtime" :
                       _type == "Accessed" ? "atime" :
                       _type == "Modified" ? "mtime" : "" ;

    return QString("%1 between \'%2\' and \'%3\'").arg(timeType, _startTime, _endTime);
}

QString SizeFilter::getType() const
{
    return "SizeFilter";
}

QString SizeFilter::getParamsQuery() const
{
    return QString ("size between \'%1\' and \'%2\'").arg(_startSize, _endSize);
}

QString HashFilter::getType() const
{
    return "HashFilter";
}

QString HashFilter::getParamsQuery() const
{
    return QString("hash = \'%1\'").arg(_hash);
}

QString ExtensionFilter::getType() const
{
    return "ExtensionFilter";
}

QString ExtensionFilter::getParamsQuery() const
{
    QStringList extensionList = _extensions.split('|');
    std::stringstream ss;
    ss << "name ilike ";
    for (int i = 0; i < extensionList.size(); ++i) {
        ss << "\'%." << (extensionList.at(i)).toStdString() << "\'";
        if (i != extensionList.size() - 1) {
            ss << " or name ilike ";
        }
    }
    return QString::fromStdString(ss.str());
}

QString NameFilter::getType() const
{
    return "NameFilter";
}

QString NameFilter::getParamsQuery() const
{
    return QString("name ~ \'%1\'").arg(_namePattern);
}

FiltersModel::FiltersModel(QStringListModel *parent) :
    QStringListModel(parent)
{

}

QString FiltersModel::getSearchParams()
{
    QMap<QString, QStringList> paramsForFilter;
    for (auto key : _filters.keys()) {
        auto filter = _filters.value(key);
        paramsForFilter[filter->getType()].append(filter->getParamsQuery());
    }

    if (paramsForFilter.empty()) {
        return QString();
    }

    QStringList searchParams;
    for (auto key : paramsForFilter.keys()) {
        QStringList filterParamsList = paramsForFilter.value(key);
        QString filterParams = filterParamsList.join(" or ");
        filterParams.push_front('(');
        filterParams.push_back(')');
        searchParams.append(filterParams);
    }

    return searchParams.join(" and ");
}

void FiltersModel::appendTimeFilter(QString event, QString startTime, QString endTime)
{
    QString description = QString("%1 start: %2, end: %3").arg(event, startTime, endTime);
    _filters[description] = std::make_shared<TimeFilter>(event, startTime, endTime);

    appendFilter(description);
}

void FiltersModel::appendSizeFilter(QString startSize, QString endSize)
{
    QString description = QString("Size start: %1, end: %2").arg(startSize, endSize);
    _filters[description] = std::make_shared<SizeFilter>(startSize, endSize);

    appendFilter(description);
}

void FiltersModel::appendHashFilter(QString hash)
{
    QString description = QString("Md5: %1").arg(hash);
    _filters[description] = std::make_shared<HashFilter>(hash);

    appendFilter(description);
}

void FiltersModel::appendExtensionFilter(QString extension)
{
    QString description = QString("Extensions: %1").arg(extension);
    _filters[description] = std::make_shared<ExtensionFilter>(extension);

    appendFilter(description);
}

void FiltersModel::appendNameFilter(QString namePattern)
{
    QString description = QString("Pattern: %1").arg(namePattern);
    _filters[description] = std::make_shared<NameFilter>(namePattern);

    appendFilter(description);
}

void FiltersModel::appendFilter(QString description)
{
    _filtersList.append(description);
    setStringList(_filtersList);
}

void FiltersModel::removeFilter(int index)
{
    QString description = _filtersList.at(index);

    _filters.remove(description);

    _filtersList.removeAt(index);
    setStringList(_filtersList);
}
