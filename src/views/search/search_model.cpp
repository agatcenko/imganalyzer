#include "search_model.h"

SearchModel::SearchModel(DBParser *dbParser, QObject *parent) :
    _dbParser(dbParser), _filtersModel(new FiltersModel()), QAbstractTableModel(parent)
{
    _dbParser->truncateSearchTable();
}

SearchModel::~SearchModel()
{
    delete _filtersModel;
}

QHash<int, QByteArray> SearchModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";

    return roles;
}

QVariant SearchModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    if (role > Qt::UserRole) {
        _dbParser->getSearchResultRecord(_currentData, index.row());
        return _currentData.at(role - Qt::UserRole - 1);
    }

    return QVariant();
}

int SearchModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _dbParser->getSearchResultSize();
}

int SearchModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 2;
}

FiltersModel* SearchModel::filtersModel() const
{
    return _filtersModel;
}

void SearchModel::setFiltersModel(FiltersModel* filters)
{
    if (_filtersModel != filters) {
        _filtersModel = filters;
        emit filtersModelChanged();
    }
}

void SearchModel::search()
{
    QString searchParams = _filtersModel->getSearchParams();

    if (!searchParams.size()) {
        return;
    }

    beginResetModel();
    _dbParser->getNewSearch(searchParams);
    endResetModel();
}

int SearchModel::getFileId(int idx) const
{
    return data(index(idx, 0), IdRole).toInt();
}

void SearchModel::addTimeFilter(QString event, QString startTime, QString endTime)
{
    _filtersModel->appendTimeFilter(event, startTime, endTime);
}

void SearchModel::addSizeFilter(QString startSize, QString endSize)
{
    _filtersModel->appendSizeFilter(startSize, endSize);
}

void SearchModel::addHashFilter(QString hash)
{
    _filtersModel->appendHashFilter(hash);
}

void SearchModel::addExtensionFilter(QString extension)
{
    _filtersModel->appendExtensionFilter(extension);
}

void SearchModel::addNameFilter(QString namePattern)
{
    _filtersModel->appendNameFilter(namePattern);
}
