#pragma once

#include <memory>
#include <QObject>
#include <QStringListModel>

class Filter
{
public:
    virtual QString getType() const = 0;
    virtual QString getParamsQuery() const = 0;
    virtual ~Filter() {}
};

class TimeFilter : public Filter
{
public:
    TimeFilter(QString type = "", QString start = "", QString end = "") :
        _type(type), _startTime(start), _endTime(end) {}

    QString getType() const;
    QString getParamsQuery() const;

private:
    QString _type;
    QString _startTime;
    QString _endTime;
};

class SizeFilter : public Filter
{
public:
    SizeFilter(QString start = "", QString end = "") :
        _startSize(start), _endSize(end) {}

    QString getType() const;
    QString getParamsQuery() const;

private:
    QString _startSize;
    QString _endSize;
};

class HashFilter : public Filter
{
public:
    HashFilter(QString hash = "") : _hash(hash) {}

    QString getType() const;
    QString getParamsQuery() const;

private:
    QString _hash;
};

class ExtensionFilter : public Filter
{
public:
    ExtensionFilter(QString extensions = ""):
        _extensions(extensions) {}

    QString getType() const;
    QString getParamsQuery() const;

private:
    QString _extensions;
};

class NameFilter : public Filter
{
public:
    NameFilter(QString namePattern = "") :
        _namePattern(namePattern) {}

    QString getType() const;
    QString getParamsQuery() const;

private:
    QString _namePattern;
};

class FiltersModel : public QStringListModel
{
    Q_OBJECT
public:
    FiltersModel(QStringListModel* parent = 0);

    QString getSearchParams();
    void appendTimeFilter(QString event, QString startTime, QString endTime);
    void appendSizeFilter(QString startSize, QString endSize);
    void appendHashFilter(QString hash);
    void appendExtensionFilter(QString extension);
    void appendNameFilter(QString namePattern);

    Q_INVOKABLE void removeFilter(int index);

private:
    QStringList _filtersList;
    QMap<QString, std::shared_ptr<Filter> > _filters;

    void appendFilter(QString description);
};
