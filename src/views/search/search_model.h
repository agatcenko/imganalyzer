#pragma once

#include "filters.h"
#include "../../data_base/db_parser.h"

#include <QAbstractTableModel>

class SearchModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(FiltersModel* filtersModel READ filtersModel WRITE setFiltersModel NOTIFY filtersModelChanged)
public:
    explicit SearchModel(DBParser* dbParser, QObject *parent = 0);
    virtual ~SearchModel();

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    enum TableRoles {
        IdRole = Qt::UserRole + 1,
        NameRole
    };

    FiltersModel* filtersModel() const;
    void setFiltersModel(FiltersModel* filters);

    Q_INVOKABLE void search();
    Q_INVOKABLE int getFileId(int idx) const;
    Q_INVOKABLE void addSizeFilter(QString startSize, QString endSize);
    Q_INVOKABLE void addTimeFilter(QString event, QString startTime, QString endTime);
    Q_INVOKABLE void addHashFilter(QString hash);
    Q_INVOKABLE void addExtensionFilter(QString extension);
    Q_INVOKABLE void addNameFilter(QString namePattern);

signals:
    void filtersModelChanged();

private:
    DBParser* _dbParser;
    FiltersModel* _filtersModel;
    mutable SearchData _currentData;
};
