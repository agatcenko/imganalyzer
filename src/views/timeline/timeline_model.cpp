#include "timeline_model.h"

TimeLineModel::TimeLineModel(DBParser *dbParser, QObject* parent) :
    QAbstractTableModel(parent), _dbParser(dbParser)
{

}

TimeLineModel::~TimeLineModel()
{

}

QVariant TimeLineModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() >= _numRow)
        return QVariant();

    if (role > Qt::UserRole) {
        if (_currentData.row != index.row()) {
            _dbParser->getTLRecord(_currentData, index.row());
        }
        return _currentData.at(role - Qt::UserRole - 1);
    }

    return QVariant();
}

int TimeLineModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (_numRow == -1)
        _numRow = _dbParser->getTLRecordsNum();

    return _numRow;
}

int TimeLineModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (_numCol == -1)
        _numCol = TimeLineData::size;

    return _numCol;
}

QHash<int, QByteArray> TimeLineModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[TimeRole] = "time";
    roles[FileRole] = "file";
    roles[EventRole] = "event";
    return roles;
}

QVariantMap TimeLineModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys()) {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }

    return map;
}

int TimeLineModel::getFileId(int idx) const
{
    QVariantMap m = get(idx);
    return m[roleNames().value(IdRole)].toInt();
}
