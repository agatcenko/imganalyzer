#pragma once

#include "../../data_base/db_parser.h"

#include <QAbstractTableModel>

class TimeLineModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit TimeLineModel(DBParser* dbParser, QObject* parent = 0);
    ~TimeLineModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE QVariantMap get(int idx) const;
    Q_INVOKABLE int getFileId(int idx) const;

    enum TableRoles {
        IdRole = Qt::UserRole + 1,
        TimeRole,
        FileRole,
        EventRole
    };

private:
    mutable int _numRow = -1;
    mutable int _numCol = -1;

    DBParser* _dbParser;
    mutable TimeLineData _currentData;
};
