#include "file_string_view.h"

#include <QTextCodec>
#include <QTextStream>

FileStringView::FileStringView(ContentStorage* contentStorage, QObject* parent) :
    _contentStorage(contentStorage), QObject(parent)
{
    QList<QByteArray> allCodecs = QTextCodec::availableCodecs();
    foreach (auto codecName, allCodecs) {
        _codecsList.append(codecName);
    }

    if (allCodecs.size()) {
        _currentCodec = _codecsList[0];
    }

    emit codecsListChanged();
}

FileStringView::~FileStringView()
{

}

QString FileStringView::content() const
{
    return _content;
}

void FileStringView::setContent(const QString &content)
{
    if (content != _content) {
        _content = content;
        emit contentChanged();
    }
}

QStringList FileStringView::codecsList() const
{
    return _codecsList;
}

void FileStringView::setCodecsList(const QStringList &codecsList)
{
    if (codecsList != _codecsList) {
        _codecsList = codecsList;
        emit codecsListChanged();
    }
}

void FileStringView::currentCodecChanged(QString codecName)
{
    if (_currentCodec != codecName) {
        _currentCodec = codecName;
        onContentChanged();
    }
}

void FileStringView::onContentChanged()
{
    QByteArray encodedSting = _contentStorage->getContentString();

    QTextCodec *codec = QTextCodec::codecForName(_currentCodec.toLocal8Bit());
    QTextStream in(&encodedSting);
    in.setAutoDetectUnicode(false);
    in.setCodec(codec);
    QString str = in.readAll();
    _content = str;

    emit contentChanged();
}
