#pragma once

#include "../../content_utils/content_storage.h"
#include "../../data_base/db_parser.h"

#include <QObject>

class FileStringView : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString content READ content WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(QStringList codecsList READ codecsList WRITE setCodecsList NOTIFY codecsListChanged)

public:
    explicit FileStringView(ContentStorage* contentStorage, QObject* parent = 0);
    virtual ~FileStringView();

    QString content() const;
    void setContent(const QString &content);

    QStringList codecsList() const;
    void setCodecsList(const QStringList &codecsList);

    Q_INVOKABLE void currentCodecChanged(QString codecName);

signals:
    void contentChanged();
    void codecsListChanged();

public slots:
    void onContentChanged();

private:
    QString _content;
    QString _currentCodec;
    QStringList _codecsList;
    ContentStorage* _contentStorage;
};
