#pragma once

#include "../../qml_macro.h"
#include "../../data_base/db_parser.h"

#include <QObject>

class FileInfoView : public QObject
{
    Q_OBJECT

public:
    FileInfoView(DBParser* dbParser, QObject* parent = 0);
    ~FileInfoView();

    Q_INVOKABLE void getInfo(long id);

signals:
    void nameChanged();
    void atimeChanged();
    void ctimeChanged();
    void mtimeChanged();
    void crtimeChanged();
    void pathChanged();
    void hashChanged();
    void metaFlagChanged();
    void metaModeChanged();
    void metaTypeChanged();
    void uidChanged();
    void gidChanged();
    void sizeChanged();

private:
    REGISTER_QML_VALUE(QString, name)
    REGISTER_QML_VALUE(QString, atime)
    REGISTER_QML_VALUE(QString, ctime)
    REGISTER_QML_VALUE(QString, mtime)
    REGISTER_QML_VALUE(QString, crtime)
    REGISTER_QML_VALUE(QString, path)
    REGISTER_QML_VALUE(QString, hash)
    REGISTER_QML_VALUE(QString, metaFlag)
    REGISTER_QML_VALUE(QString, metaMode)
    REGISTER_QML_VALUE(QString, metaType)
    REGISTER_QML_VALUE(int, uid)
    REGISTER_QML_VALUE(int, gid)
    REGISTER_QML_VALUE(double, size)

    FileInfo _info;
    DBParser* _dbParser;
 };
