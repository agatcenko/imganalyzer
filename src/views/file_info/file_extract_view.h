#pragma once

#include "../../qml_macro.h"
#include "../../data_base/db_parser.h"
#include "../../content_utils/content_saver.h"

#include <QObject>

class FileExtractView : public QObject
{
    Q_OBJECT

public:
    explicit FileExtractView(DBParser *dbParser, QObject *parent = 0);
    virtual ~FileExtractView();

    Q_INVOKABLE void changeExtractiveFile(int fileId);
    Q_INVOKABLE void extractFiles(QString outputDir);

public slots:
    void setExtractionProgress(QString name);
    void setExtractionEnd();

signals:
    void extractionInfoChanged();
    void extractionProgressChanged();

private:
    int _currentFileCounter = 1;
    int _filesToExtractCounter = 0;
    ContentSaver* _contentSaver = nullptr;

    REGISTER_QML_VALUE(QString, extractionInfo)
    REGISTER_QML_VALUE(QString, extractionProgress)

    void getExtractionInfo();
};
