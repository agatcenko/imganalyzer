#pragma once

#include "../../qml_macro.h"
#include "../../content_utils/content_storage.h"

#include <QAbstractListModel>

class FileHexView : public QAbstractListModel
{
    Q_OBJECT

    Q_ENUMS(Param)
    Q_PROPERTY (QString address READ address WRITE setAddress NOTIFY addressChanged)

public:
    explicit FileHexView(ContentStorage *contentStorage, QObject *parent = 0);
    virtual ~FileHexView();

    Q_INVOKABLE void pressKey(int keyCode);
    QStringList rowData(const QModelIndex &index);
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    QString address() const;
    void setAddress(const QString &addr);

    enum Param {
        LineSize = 16
    };

    enum Roles {
        Line = Qt::UserRole + 1
    };

protected:
    virtual QHash<int, QByteArray> roleNames() const;

signals:
    void offsetChanged();
    void addressChanged(int address);

public slots:
    void onContentChanged();

private:
    int _address;
    QModelIndex _currentRowIndex;
    QStringList _currentRowData;
    ContentStorage* _contentStorage;

    REGISTER_QML_VALUE(int, offset)

    bool isNavigational(int keyCode) const;
    void navigate(int keyCode);
};
