#include "file_extract_view.h"
#include "../../data_base/query_utils.h"
#include "../../config_utils/config_utils.h"

FileExtractView::FileExtractView(DBParser *dbParser, QObject *parent) :
    QObject(parent)
{
    _contentSaver = new ContentSaver(dbParser);

    QObject::connect(_contentSaver, SIGNAL(fileExtracted(QString)),
                     this, SLOT(setExtractionProgress(QString)));
    QObject::connect(_contentSaver, SIGNAL(extractionEnd()),
                     this, SLOT(setExtractionEnd()));

    setextractionInfo("Not ready to extract");
}

FileExtractView::~FileExtractView()
{
    if (_contentSaver != nullptr) {
        delete _contentSaver;
    }
}

void FileExtractView::changeExtractiveFile(int fileId)
{
    _contentSaver->setId(fileId);
    getExtractionInfo();
}

void FileExtractView::extractFiles(QString outputDir)
{
    _extractionProgress.clear();
    if (!_contentSaver->isBusy()) {
        _contentSaver->extractFiles(outputDir);
    }
}

void FileExtractView::setExtractionProgress(QString name)
{
    _extractionProgress.append(QString("(%1/%2) File %3 is extracted\n").arg(
                                   QString::number(_currentFileCounter++),
                                   QString::number(_filesToExtractCounter),  name));
    emit extractionProgressChanged();
}

void FileExtractView::setExtractionEnd()
{
    _currentFileCounter = 1;
    _extractionProgress.append("Done.");
    emit extractionProgressChanged();
}

void FileExtractView::getExtractionInfo()
{
    ExtractionInfo info = _contentSaver->getExtractionInfo();

    std::string infoString = MakeQuery() << "It`s a " << info.fileType.toStdString() <<
                                            ". Ready to extract " << info.fileCount <<
                                            " item(s). Total: " << info.contentSize << " bytes.";
    _filesToExtractCounter = info.fileCount;
    _extractionInfo = QString::fromStdString(infoString);

    emit extractionInfoChanged();
}
