#include "file_hex_view.h"

using namespace std;

FileHexView::FileHexView(ContentStorage* contentStorage, QObject *parent) :
    _contentStorage(contentStorage), QAbstractListModel(parent)
{

}

FileHexView::~FileHexView()
{

}

QStringList FileHexView::rowData(const QModelIndex &index)
{
    if (_currentRowIndex == index && !_currentRowData.empty()) {
        return _currentRowData;
    }

    _currentRowIndex = index;
    _currentRowData.clear();

    int offset = index.row() * LineSize;
    QString addr = QString("%1").arg(((_contentStorage->currentPage() - 1) * _contentStorage->PAGE_SIZE + offset),
                                     8, 16, QChar('0').toUpper());
    _currentRowData.append(addr);
    for (auto i = 0; i < LineSize; ++i) {
        _currentRowData.append(QString("%1").arg((quint8)_contentStorage->getContentAt(offset + i),
                         2, 16, QChar('0').toUpper()));
    }

    for (auto i = 0; i < LineSize; ++i) {
        _currentRowData.append(QString(((quint8)_contentStorage->getContentAt(offset + i))).toLocal8Bit());
    }

    return _currentRowData;
}

QVariant FileHexView::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (role > Qt::UserRole) {
        return const_cast<FileHexView*>(this)->rowData(index);
    }

    return QVariant();
}

int FileHexView::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return _contentStorage->PAGE_SIZE / LineSize;
}

QString FileHexView::address() const
{
    return QString("%1").arg(_address, 8, 16, QChar('0').toUpper());
}

void FileHexView::setAddress(const QString &addr)
{
    int address = addr.toInt(0, 16);
    _address = address / LineSize * LineSize;
    addressChanged(_address);
}

QHash<int, QByteArray> FileHexView::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Line] = "lineData";

    return roles;
}

void FileHexView::pressKey(int keyCode)
{
    if (isNavigational(keyCode)) {
        navigate(keyCode);
    }
}

void FileHexView::onContentChanged()
{
    beginResetModel();
    endResetModel();
}

bool FileHexView::isNavigational(int keyCode) const
{
    return (keyCode == Qt::Key_Left) || (keyCode == Qt::Key_Right) ||
            (keyCode == Qt::Key_Up)  || (keyCode == Qt::Key_Down);
}

void FileHexView::navigate(int keyCode)
{
    switch (keyCode) {
        case Qt::Key_Left:
            if (_offset > 1) {
                setoffset(--_offset);
                offsetChanged();
            }
            break;
        case Qt::Key_Right:
            if (_offset < LineSize) {
                setoffset(++_offset);
                offsetChanged();
            }
            break;
        case Qt::Key_Up:
            if (_address >= LineSize + _contentStorage->PAGE_SIZE * (_contentStorage->currentPage() - 1)) {
                _address -= LineSize;
            }
            break;
        case Qt::Key_Down:
            if (_address + LineSize < _contentStorage->PAGE_SIZE * (_contentStorage->currentPage())) {
                _address += LineSize;
            }
            break;
    }

    emit addressChanged(_address);
}
