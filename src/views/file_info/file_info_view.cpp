#include "file_info_view.h"

FileInfoView::FileInfoView(DBParser* dbParser, QObject* parent) :
    _dbParser(dbParser), QObject(parent)
{

}

FileInfoView::~FileInfoView()
{

}

void FileInfoView::getInfo(long id)
{
    _dbParser->getFileInfo(_info, id);

    setname(QString::fromStdString(_info.name));
    setpath(QString::fromStdString(_info.path));
    sethash(QString::fromStdString(_info.hash));
    setatime(QString::fromStdString(_info.atime));
    setctime(QString::fromStdString(_info.ctime));
    setmtime(QString::fromStdString(_info.mtime));
    setcrtime(QString::fromStdString(_info.crtime));
    setmetaFlag(QString::fromStdString(metaFlagEnumToString(_info.flags)));
    setmetaMode(QString::fromStdString(metaModeEnumToString(_info.mode)));
    setmetaType(QString::fromStdString(metaTypeEnumToString(_info.type)));
    setuid(_info.uid);
    setgid(_info.gid);
    setsize(_info.size);
}
