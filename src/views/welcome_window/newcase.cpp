#include "newcase.h"

#include <string>

NewCase::NewCase(QObject *parent) :
    QObject(parent)
{

}

NewCase::~NewCase()
{
    if (db != nullptr)
        delete db;
}

QString NewCase::path() const
{
    return m_path;
}

QString NewCase::name() const
{
    return m_name;
}

bool NewCase::rewriteFlag() const
{
    return m_rewriteFlag;
}

void NewCase::setPath(const QString &path)
{
    if (path != m_path) {
        m_path = path;
        emit pathChanged();
    }
}

void NewCase::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
        emit nameChanged();
    }
}

void NewCase::setRewriteFlag(const bool &flag)
{
    if (flag != m_rewriteFlag) {
        m_rewriteFlag = flag;
        emit rewriteFlagChanged();
    }
}

bool NewCase::createCase()
{
    if (db != nullptr)
        delete db;
    db = new DBCreator(m_name.toStdString());
    db->allowUseExistDataBase(m_rewriteFlag);
    if (db->connect()) {
        return false;
    }

    startAnalyzeImage(db);
    return true;
}

void NewCase::startAnalyzeImage(DBCreator *db)
{
    t = std::thread(&NewCase::analyzeImage, this, db);
    t.detach();
}

void NewCase::analyzeImage(DBCreator *db)
{
    ImgAnalyzer analyzer(db);
    TSK_TCHAR** img_pathes = new TSK_TCHAR*[1];
    img_pathes[0] = new TSK_TCHAR[m_path.size() + 1];
    strcpy(img_pathes[0], m_path.toStdString().c_str());
    img_pathes[0][m_path.size()] = (TSK_TCHAR)'\0';

    analyzer.run(1, img_pathes);

    delete[] img_pathes;

    DBCreator historyCreator("cases_history");
    historyCreator.updateHistory(m_name.toStdString(), m_path.toStdString());

    emit caseCreated();
}

bool NewCase::isAllFilled()
{
    if (m_path != "" && m_name != "")
        return true;
    else
        return false;
}

QString NewCase::nameFromPath(const QString &path)
{
    std::string path_str = path.toStdString();
    size_t path_separator = path_str.find_last_of("/\\");
    size_t extension_separator = path_str.find_last_of('.');
    path_str = path_str.substr(path_separator + 1, extension_separator - path_separator - 1);

    return QString::fromStdString(path_str);
}
