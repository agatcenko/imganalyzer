#include "lastcase.h"
#include "../../config_utils/config_utils.h"

LastCase::LastCase(QObject *parent) :
    QObject(parent)
{
}

LastCase::~LastCase()
{
}

int LastCase::readLastCaseConfig()
{
    _dbName = QString::fromStdString(config::getCurrentDataBaseName());

    if (_dbName.size() == 0) {
        return 1;
    }

    return 0;
}
