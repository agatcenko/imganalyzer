#pragma once

#include "../../data_base/db_creator.h"
#include "../../image_analyzer.h"

#include <QObject>
#include <QVariant>
#include <thread>

class NewCase : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool rewriteFlag READ rewriteFlag WRITE setRewriteFlag NOTIFY rewriteFlagChanged)

public:
    NewCase(QObject *parent = 0);
    ~NewCase();

    QString path() const;
    QString name() const;
    bool rewriteFlag() const;

    void setPath(const QString &path);
    void setName(const QString &name);
    void setRewriteFlag(const bool &flag);

    Q_INVOKABLE bool createCase();
    Q_INVOKABLE bool isAllFilled();
    Q_INVOKABLE QString nameFromPath(const QString &path);

signals:
    void pathChanged();
    void nameChanged();
    void caseCreated();
    void rewriteFlagChanged();

private:
    QString m_path;
    QString m_name;
    bool m_rewriteFlag;

    std::thread t;
    DBCreator *db = nullptr;

    void startAnalyzeImage(DBCreator *db);
    void analyzeImage(DBCreator *db);
};
