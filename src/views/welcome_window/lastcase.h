#pragma once

#include "../../qml_macro.h"
#include <QObject>

class LastCase : public QObject
{
    Q_OBJECT

signals:
    void dbNameChanged();

public:
    LastCase(QObject *parent = 0);
    ~LastCase();

    Q_INVOKABLE int readLastCaseConfig();

    REGISTER_QML_VALUE(QString, dbName)
};
