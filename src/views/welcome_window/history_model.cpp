#include "history_model.h"

HistoryModel::HistoryModel(QAbstractTableModel *parent) :
    QAbstractTableModel(parent)
{
    _historyParser = new DBParser();
    _historyParser->setDBName("history_db");
    _historyParser->connect();
}

HistoryModel::~HistoryModel()
{
    delete _historyParser;
}

QHash<int, QByteArray> HistoryModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[PathRole] = "path";

    return roles;
}

QVariant HistoryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role > Qt::UserRole) {
        _historyParser->getHistoryRecord(_currentData, index.row());
        return _currentData.at(role - Qt::UserRole - 1);
    }

    return QVariant();
}

int HistoryModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (_numRow == -1)
        _numRow = _historyParser->getHistoryRecordsNum();

    return _numRow;
}

int HistoryModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (_numCol == -1)
        _numCol = HistoryData::size;

    return _numCol;
}

bool HistoryModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    for (int i = 0; i < count; ++i) {
        int caseId = getCaseId(row + i);
        _historyParser->removeHistoryRow(caseId);
    }
    endRemoveRows();

    return true;
}

void HistoryModel::removeCase(int row)
{
    removeRows(row, 1, QModelIndex());
}

QString HistoryModel::getCaseName(int row)
{
    return data(index(row, 1), NameRole).toString();
}

int HistoryModel::getCaseId(int row) const
{
    return data(index(row, 0), IdRole).toInt();
}
