#pragma once

#include "../../data_base/db_types.h"
#include "../../data_base/db_parser.h"

#include <QAbstractTableModel>

class HistoryModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit HistoryModel(QAbstractTableModel *parent = 0);
    virtual ~HistoryModel();

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    bool removeRows(int row, int count, const QModelIndex &parent) Q_DECL_OVERRIDE;

    Q_INVOKABLE void removeCase(int row);
    Q_INVOKABLE QString getCaseName(int row);

    enum TableRoles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        PathRole
    };

private:
    mutable int _numRow = -1;
    mutable int _numCol = -1;
    mutable HistoryData _currentData;
    DBParser* _historyParser;

    int getCaseId(int row) const;
};
