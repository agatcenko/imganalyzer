#include "tree_item.h"

TreeItem::TreeItem(long id, const QVector<QVariant> &data, QVariant icon, TreeItem *parent) :
    dbId(id), itemData(data), itemIcon(icon), parentItem(parent)
{
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

QVariant TreeItem::getItemIcon() const
{
    return itemIcon;
}

QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

TreeItem* TreeItem::parent()
{
    return parentItem;
}

TreeItem* TreeItem::child(int number)
{
    return childItems.value(number);
}

QList<TreeItem*> TreeItem::children() const
{
    return childItems;
}

QList<ItemInfo> TreeItem::getEndPointItems() const
{
    return endpointItems;
}

long TreeItem::getId() const
{
    return dbId;
}

bool TreeItem::setData(int column, const QVariant &data)
{
    if (column < 0 && column >= itemData.size())
        return false;

    itemData[column] = data;
    return true;
}

bool TreeItem::insertChildren(QVector<long> &ids, int pos, int count, int columns, const QVariant &icon)
{
    if (pos < 0 || pos > childItems.size())
        return false;
    if (ids.size() != count)
        return false;

    for (int row = 0; row < count; ++row) {
        QVector<QVariant> data(columns);
        TreeItem *item = new TreeItem(ids[row], data, icon, this);
        childItems.insert(pos, item);
    }

    return true;
}

bool TreeItem::insertChild(long id, int columns, const QVariant &data, const QVariant &icon)
{
    QVector<long> ids (1, id);
    if(!insertChildren(ids, childCount(), 1, columns, icon))
        return false;

    if(child(childCount() - 1)->setData(0, data))
        return false;

    return true;
}

void TreeItem::insertEndPointItem(long id, const QVariant &data)
{
    endpointItems.append(ItemInfo(id, data, "qrc:/images/file.png"));
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::columnCount() const
{
    return itemData.count();
}

int TreeItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

bool TreeItem::isChildrenLoaded() const
{
    return childrenLoaded;
}

void TreeItem::loadChildren()
{
    childrenLoaded = true;
}
