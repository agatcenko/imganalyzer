#pragma once

#include "tree_item.h"
#include "children_table_model.h"
#include "../../data_base/db_parser.h"

#include <string>
#include <QAbstractItemModel>

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

    Q_PROPERTY(ChildrenModel* childrenModel READ childrenModel)
public:
    explicit TreeModel(DBParser* dbParser, QObject *parent = 0);
    ~TreeModel();

    enum TreeRoles {
        NameRole = Qt::UserRole + 1
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    Q_INVOKABLE long getFileId(QModelIndex index);

    ChildrenModel* childrenModel() const;

signals:
    void itemChildrenChanged();

private:
    TreeItem* rootItem;
    DBParser* _dbParser;
    ChildrenModel* _childrenModel;

    void initRootItem();
    void insertModelData(TreeItem *parent) const;
    TreeItem* getItem(const QModelIndex &idx) const;

    void updateChildrenModel(TreeItem* parent);
};
