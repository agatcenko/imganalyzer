#pragma once

#include <QMap>
#include <QList>
#include <QVector>
#include <QVariant>

struct ItemInfo
{
    long _id;
    QVariant _name;
    QVariant _image;

    ItemInfo(long id, QVariant name, QVariant image) :
        _id(id), _name(name), _image(image) {}

    const QMap<QString, QVariant> toMap() const
    {
        QMap<QString, QVariant> result;
        result.insert("name", _name);
        result.insert("image", _image);
        return result;
    }
};

class TreeItem
{
public:
    explicit TreeItem(long id, const QVector<QVariant> &data, QVariant icon, TreeItem *parent = 0);
    ~TreeItem();

    QVariant getItemIcon() const;
    QVariant data(int column) const;
    TreeItem* parent();
    TreeItem* child(int number);
    QList<TreeItem*> children() const;
    QList<ItemInfo> getEndPointItems() const;

    long getId() const;
    bool setData(int column, const QVariant &data);
    bool insertChildren(QVector<long> &ids, int pos, int count, int columns, const QVariant &icon);

    bool insertChild(long id, int columns, const QVariant &data, const QVariant &icon);
    void insertEndPointItem(long id, const QVariant &data);

    int childCount() const;
    int columnCount() const;
    int childNumber() const;

    bool isChildrenLoaded() const;
    void loadChildren();

private:
    QList<TreeItem*> childItems;
    QList<ItemInfo> endpointItems; // only for files without children
    QVector<QVariant> itemData;
    TreeItem* parentItem;
    QVariant itemIcon;

    long dbId;
    bool childrenLoaded = false;
};
