#include "children_table_model.h"

ChildrenModel::ChildrenModel(QAbstractTableModel *parent) :
    QAbstractTableModel(parent)
{
}

ChildrenModel::~ChildrenModel()
{

}

QHash<int, QByteArray> ChildrenModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";

    return roles;
}

QVariant ChildrenModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == NameRole) {
        return QVariant(_children[index.row()].toMap());
    }

    return QVariant();
}

int ChildrenModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _children.size();
}

int ChildrenModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 1; // show only name
}

void ChildrenModel::updateData(QList<ItemInfo> &data)
{
    beginResetModel();
    _children = data;
    endResetModel();
}

long ChildrenModel::getFileId(int index)
{
    if (index >= 0 && index < _children.size()) {
        return _children[index]._id;
    }

    return -1;
}
