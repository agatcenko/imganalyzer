#pragma once

#include "tree_item.h"

#include <QAbstractTableModel>

class ChildrenModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ChildrenModel(QAbstractTableModel *parent = 0);
    virtual ~ChildrenModel();

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;

    enum TableRoles {
        NameRole = Qt::UserRole + 1,
    };

    Q_INVOKABLE long getFileId(int index);

    void updateData(QList<ItemInfo> &data);

private:
    QList<ItemInfo> _children;
};
