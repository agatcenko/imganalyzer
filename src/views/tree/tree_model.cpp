#include "tree_model.h"
#include "../../config_utils/yaml_reader.h"
#include "../../data_base/query_utils.h"

#include <iostream>

TreeModel::TreeModel(DBParser* dbParser, QObject *parent) :
    QAbstractItemModel(parent), _dbParser(dbParser), _childrenModel(new ChildrenModel())
{
    QStringList headers;
    headers << tr("Object Name");

    QVector<QVariant> rootData;
    foreach (QString header, headers) {
        rootData << header;
    }

    rootItem = new TreeItem(-1, rootData, "");

    initRootItem();
    insertModelData(rootItem->child(0));
}

TreeModel::~TreeModel()
{
    delete rootItem;
    delete _childrenModel;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != NameRole)
       return QVariant();

    TreeItem *item = getItem(index);

    QMap<QString, QVariant> resData;
    resData.insert("name", item->data(index.column()));
    resData.insert("image", item->getItemIcon());

    return QVariant(resData);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeItem *parentItem = getItem(parent);
    TreeItem *childItem = parentItem->child(row);

    if (childItem) {
        if (!childItem->isChildrenLoaded())
            insertModelData(childItem);
        return createIndex(row, column, childItem);
    }
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &child) const
{
    if (!child.isValid())
        return QModelIndex();

    TreeItem *childItem = getItem(child);
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

QHash<int, QByteArray> TreeModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    return roles;
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem = getItem(parent);

    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return rootItem->columnCount();
}

void TreeModel::initRootItem()
{
    QVariant rootData("Tree View");
    QVariant rootChildData("Image");
    rootItem->insertChild(-1, rootItem->columnCount(), rootData, "");
    TreeItem* child = rootItem->child(0);
    child->insertChild(0, rootItem->columnCount(), rootChildData, "");
}

void TreeModel::insertModelData(TreeItem *parent) const
{
    std::vector<std::pair<long, DB_OBJECT_TYPE> > children;
    if (_dbParser->getChildrenIds(parent->getId(), children)) {
        std::cout << "getChildrenIds failed" << std::endl;
        return;
    }

    for (const auto & child : children) {
        QVariant insertData, iconImage;
        if (child.second == DB_OBJECT_TYPE_IMAGE) {
            ImageInfo imgInfo;
            if (_dbParser->getImageInfo(imgInfo, child.first))
                continue;

            insertData = QVariant::fromValue(QString::fromStdString(imgInfo.name));
            iconImage = "qrc:/images/disk.png";
        }
        else if (child.second == DB_OBJECT_TYPE_VS) {
            VsInfo vsInfo;
            if (_dbParser->getVsInfo(vsInfo, child.first))
                continue;

            insertData = QVariant::fromValue(QString::fromStdString(tsk_vs_type_toname(vsInfo.vstype)));
            iconImage = "qrc:/images/volume_system.png";
        }
        else if (child.second == DB_OBJECT_TYPE_VOL) {
            VolInfo volInfo;
            if (_dbParser->getVolInfo(volInfo, child.first))
                continue;
            std::string data = MakeQuery() << "vol" << volInfo.addr <<
                                              " (" << volInfo.desc << ")";
            insertData = QVariant::fromValue(QString::fromStdString(data));
            iconImage = "qrc:/images/volume.png";
        }
        else if (child.second == DB_OBJECT_TYPE_FS) {
            FsInfo fsInfo;
            if (_dbParser->getFsInfo(fsInfo, child.first))
                continue;

            insertData = QVariant::fromValue(QString::fromStdString(tsk_fs_type_toname(fsInfo.type)));
            iconImage = "qrc:/images/filesystem.png";
        }
        else if (child.second == DB_OBJECT_TYPE_FILE) {
            FileInfo fileInfo;
            if (_dbParser->getFileInfo(fileInfo, child.first))
                continue;

            insertData = QVariant::fromValue(QString::fromStdString(fileInfo.name));
            iconImage = "qrc:/images/folder.png";

            if (fileInfo.type != TSK_FS_META_TYPE_DIR) {
                parent->insertEndPointItem(child.first, insertData);
                continue;
            }
        }
        parent->insertChild(child.first, rootItem->columnCount(), insertData, iconImage);
    }

    parent->loadChildren();
}

TreeItem* TreeModel::getItem(const QModelIndex &idx) const
{
    if (idx.isValid()) {
        TreeItem *item = static_cast<TreeItem*>(idx.internalPointer());
        if (item) {
            return item;
        }
    }

    return rootItem;
}

long TreeModel::getFileId(QModelIndex index)
{
    TreeItem* item = getItem(index);
    updateChildrenModel(item);

    return item->getId();
}

ChildrenModel *TreeModel::childrenModel() const
{
    return _childrenModel;
}

void TreeModel::updateChildrenModel(TreeItem *parent)
{
    QList<TreeItem*> children = parent->children();
    QList<ItemInfo> childrenInfo;
    for (auto child : children) {
        childrenInfo.push_back(ItemInfo(child->getId(), child->data(0), child->getItemIcon()));
    }

    childrenInfo.append(parent->getEndPointItems());

    _childrenModel->updateData(childrenInfo);
}
