#pragma once

#define REGISTER_QML_VALUE(ValueType, ValueName) \
    private: \
    ValueType _##ValueName; \
    Q_PROPERTY(ValueType ValueName READ ValueName WRITE set##ValueName NOTIFY ValueName##Changed) \
    public: \
    ValueType ValueName() const { return _##ValueName; } \
    void set##ValueName(const ValueType &ValueName) { \
    if (ValueName != _##ValueName) { \
        _##ValueName = ValueName; \
        emit ValueName##Changed(); \
        } \
    }
