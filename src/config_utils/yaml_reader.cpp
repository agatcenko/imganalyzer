#include "yaml_reader.h"

using namespace std;
using namespace YAML;

YamlReader::YamlReader()
{
    
}

YamlReader::YamlReader(Node config)
{
    _config = config;
}

YamlReader::YamlReader(string filename)
{
    _config = LoadFile(filename);
}

void YamlReader::setSource(std::string fileName)
{
    _config = LoadFile(fileName);
}
