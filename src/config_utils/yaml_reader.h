#pragma once

#include <yaml-cpp/yaml.h>

#include <string>
#include <iostream>

namespace {
    template <typename T>
    T get_as(const YAML::Node &node)
    {
        return node.as<T>();
    }

    template<>
    YAML::Node get_as(const YAML::Node &node)
    {
        if (!node.IsDefined())
            throw std::runtime_error("node is not deined");

        return node;
    }
}

class YamlReader
{
public:
    YamlReader();
    YamlReader(YAML::Node config);
    YamlReader(std::string fileName);

    void setSource(std::string fileName);

    template<typename T>
    bool isExists(T &val, std::string param)
    {
        try {
            val = get_as<T>(_config[param]);
        }
        catch (const std::runtime_error &e) {
            return false;
        }

        return true;
    }

    template <typename T>
    void read(T &val, std::string param)
    {
        if (!isExists(val, param)) {
            std::cout << "Param " << param << " couldn`t be read" << std::endl;
            val = T();
        }
    }

private:
    YAML::Node _config;
};
