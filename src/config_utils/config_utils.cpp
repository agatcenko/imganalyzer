#include "config_utils.h"
#include "yaml_reader.h"

#include <iostream>

using namespace std;

string config::getCurrentDataBaseName()
{
    string dbName;
    const string configName = "config/data_base_current.yml";
    YamlReader configReader(configName);

    try {
        configReader.read(dbName, "db_name");
    }
    catch (const YAML::BadFile &e) {
        cout << e.what() << endl;
        cout << "Config file " << configName << " does not exists" << endl;
        return string();
    }

    return dbName;
}
