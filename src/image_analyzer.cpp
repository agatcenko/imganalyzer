#include "image_analyzer.h"

ImgAnalyzer::ImgAnalyzer(DBCreator* db)
{
    _db = db;

    _isVsFound = false;
    _isVolFound = false;
}

ImgAnalyzer::~ImgAnalyzer()
{
    closeImage();
}

int ImgAnalyzer::run(int num, const TSK_TCHAR* const images[])
{
    _objectId = 1;

    if (openImageUtf8(num, images, TSK_IMG_TYPE_DETECT, 0)) {
        return 1;
    }

    int ret = findFilesInImg();
    if (_db->getQueueSize()) {
        _db->executeAllQueue();
    }

    _db->createTimeLine();

    QString fileName = "config/data_base_current.yml";
    QFile dataBaseInfo(fileName);
    if (dataBaseInfo.open(QIODevice::WriteOnly)) {
        QTextStream stream(&dataBaseInfo);
        stream << "db_name: " << QString::fromStdString(_db->getName()) << endl;
    }
    dataBaseInfo.close();

    return ret;
}

uint8_t ImgAnalyzer::openImageUtf8(int a_numImg, const char* const a_images[], 
    TSK_IMG_TYPE_ENUM a_imgType, unsigned int a_sSize)
{
    uint8_t ret = TskAuto::openImageUtf8(a_numImg, a_images, a_imgType, a_sSize);
    if (ret)
        return ret;

    return saveImgInfo(a_numImg, a_images) != TSK_OK ? 1 : 0;
}

TSK_RETVAL_ENUM ImgAnalyzer::processFile(TSK_FS_FILE* fs_file, const char* path)
{
    TSK_RETVAL_ENUM ret;

    if (isDotDir(fs_file))
        return TSK_OK;
    else if ((!fs_file->meta) || (fs_file->meta->size == 0)) {
        return TSK_OK;
    }

    if (tsk_fs_file_attr_getsize(fs_file) == 0) {
        ret = saveFileInfo(fs_file, nullptr, path);
    }
    else {
        ret = processAttributes(fs_file, path);
    }
    
    return ret == TSK_STOP ? TSK_STOP : TSK_OK;
}

TSK_RETVAL_ENUM ImgAnalyzer::processAttribute(TSK_FS_FILE* fs_file, const TSK_FS_ATTR* fs_attr, const char* path)
{
    if (isDefaultType(fs_file, fs_attr)) {
        if (saveFileInfo(fs_attr->fs_file, fs_attr, path)) {
            registerError();
            return TSK_OK;
        }
    }

    return TSK_OK;
}

TSK_FILTER_ENUM ImgAnalyzer::filterFs(TSK_FS_INFO* fs_info)
{
    int parentId;
    if (_isVsFound && _isVolFound) {
        parentId = _currentVolId;
    }
    else {
        parentId = _currentImgId;
    }
    _currentFsId = _objectId++;

    if (_db->addFileSysInfo(_currentFsId, parentId, fs_info)) {
        registerError();
        return TSK_FILTER_STOP;
    }

    TSK_FS_FILE* fs_root;
    if ((fs_root = tsk_fs_file_open(fs_info, NULL, "/")) != NULL) {
        processFile(fs_root, "");
        tsk_fs_file_close(fs_root);
        fs_root = NULL;
    }

    TSK_FS_DIR_WALK_FLAG_ENUM filterFlags = (TSK_FS_DIR_WALK_FLAG_ENUM)
        (TSK_FS_DIR_WALK_FLAG_ALLOC | TSK_FS_DIR_WALK_FLAG_UNALLOC);

    setFileFilterFlags(filterFlags);

    return TSK_FILTER_CONT;
}

TSK_FILTER_ENUM ImgAnalyzer::filterVs(const TSK_VS_INFO* vs_info)
{
    _isVsFound = true;
    _currentVsId = _objectId++;

    if (_db->addVolSysInfo(_currentVsId, _currentImgId, vs_info)) {
        registerError();
        return TSK_FILTER_STOP;
    }

    return TSK_FILTER_CONT;
}

TSK_FILTER_ENUM ImgAnalyzer::filterVol(const TSK_VS_PART_INFO* vs_part)
{
    _isVolFound = true;
    _currentVolId = _objectId++;

    if (_db->addVolInfo(_currentVolId, _currentVsId, vs_part)) {
        registerError();
        return TSK_FILTER_STOP;
    }

    return TSK_FILTER_CONT;
}

TSK_RETVAL_ENUM ImgAnalyzer::saveImgInfo(int a_numImg, const char* const a_images[])
{
    _currentImgId = _objectId++;

    if (_db->addImgInfo(_currentImgId, m_img_info, a_numImg, a_images)) {
        registerError();
        return TSK_ERR;
    }

    return TSK_OK;
}

TSK_RETVAL_ENUM ImgAnalyzer::saveFileInfo(TSK_FS_FILE* fs_file, const TSK_FS_ATTR* fs_attr, const char* path)
{
    int parentId;
    std::string pathStr = std::string(path);
    if (pathToId.count(pathStr) == 0) {
        parentId = _currentFsId;
    }
    else {
        parentId = pathToId[pathStr];
    }

    std::string hash = calculateMd5(fs_attr);

    if (_db->addFileInfo(_objectId++, _currentFsId, parentId, fs_file, fs_attr, path, hash)) {
        registerError();
        return TSK_ERR;
    }

    if (fs_file->meta->type == TSK_FS_META_TYPE_DIR) {
        std::string fullPath = std::string(path) + fs_file->name->name + "/";
        pathToId[fullPath] = _objectId - 1;
    }

    return TSK_OK;
}

uint8_t ImgAnalyzer::handleError()
{
    error_record err = getErrorList().back();

    return 0;   
}

std::string ImgAnalyzer::calculateMd5(const TSK_FS_ATTR* fsAttr)
{
    std::string hashStr (32, 0);
    static const char hexMap[] = "0123456789abcdef";

    const unsigned int BUFFER_SIZE = 32768;
    char buffer[BUFFER_SIZE];
    int bytesRead = 0;

    if (fsAttr == NULL) {
        return hashStr;
    }

    try {
        TSK_MD5_CTX md5Ctx;
        TSK_MD5_Init(&md5Ctx);
        TSK_OFF_T offset = 0;

        do {
            bytesRead = tsk_fs_attr_read(fsAttr, offset, buffer, BUFFER_SIZE, TSK_FS_FILE_READ_FLAG_NONE);
            if (bytesRead > 0) {
                offset += bytesRead;
                TSK_MD5_Update(&md5Ctx, (unsigned char *)buffer, bytesRead);
            }
        }
        while (bytesRead > 0);

        const unsigned int hashSize = 16;
        unsigned char hash[hashSize];
        TSK_MD5_Final(hash, &md5Ctx);
        for (int i = 0; i < hashSize; ++i) {
            hashStr[2 * i] = hexMap[(hash[i] >> 4) & 0xf];
            hashStr[2 * i + 1] = hexMap[hash[i] & 0xf];
        }
    }
    catch (const std::exception &e) {
        std::cout << "Erorr while calculate md5: " << e.what() << std::endl;
    }

    return hashStr;
}
