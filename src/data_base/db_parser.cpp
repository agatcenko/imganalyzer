#include "db_parser.h"
#include "query_utils.h"

#include <iostream>
#include <exception>

using namespace std;
using namespace pqxx;

DBParser::DBParser()
{

}

DBParser::~DBParser()
{
    disconnect();
}

int DBParser::connect()
{
    if (_name.empty())
        return 1;
    try {
        _con = new connection(MakeQuery() << "dbname = " << _name << " user = postgres");
    }
    catch (const broken_connection &e) {
        cout << "Failed while try to estblish connection: " << e.what() << endl;
        return 1;
    }

    getSettingsInfo(_settings);

    return _con->is_open() ? 0 : 1;
}

void DBParser::disconnect()
{
    if (_con != nullptr) {
        delete _con;
        _con = nullptr;
    }
}

void DBParser::setDBName(string name)
{
    _name = name;
}

string DBParser::getDBName() const
{
    return _name;
}

bool DBParser::isConnected() const
{
    if (_con == nullptr)
        return false;
    if (!_con->is_open())
        return false;

    return true;
}

int DBParser::getImageInfo(ImageInfo &imageInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, path, name, itype, size, sector_size FROM img_info " <<
                                            "WHERE id = " << id << ";");

        if (res.size() == 1) {
            int i = 0;
            res[0][i++].to(imageInfo.id);
            res[0][i++].to(imageInfo.path);
            res[0][i++].to(imageInfo.name);
            res[0][i++].to(imageInfo.type);
            res[0][i++].to(imageInfo.size);
            res[0][i++].to(imageInfo.sectorSize);
        }
        else {
            rowNumberException e;
            throw e;
        }
    }
    catch (const exception &e) {
        cout << "Error while getImageInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getVsInfo(VsInfo &vsInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, block_size, endian, offset_vs, part_count, vstype " <<
                             "FROM vs_info WHERE id = " << id << ";");

        if (res.size() == 1) {
            int i = 0;
            res[0][i++].to(vsInfo.id);
            res[0][i++].to(vsInfo.block_size);
            res[0][i++].to((int &)vsInfo.endian);
            res[0][i++].to(vsInfo.offset_vs);
            res[0][i++].to(vsInfo.part_count);
            res[0][i++].to((int &)vsInfo.vstype);
        }
        else {
            rowNumberException e;
            throw e;
        }
    }
    catch (const exception &e) {
        cout << "Error while getVsInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getVolInfo(VolInfo &volInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, addr, desc_vol, flags, len, start " <<
                             "FROM vol_info WHERE id = " << id << ";");

        if (res.size() == 1) {
            int i = 0;
            res[0][i++].to(volInfo.id);
            res[0][i++].to(volInfo.addr);
            res[0][i++].to(volInfo.desc);
            res[0][i++].to((int &)volInfo.flags);
            res[0][i++].to(volInfo.len);
            res[0][i++].to(volInfo.start);
        }
        else {
            rowNumberException e;
            throw e;
        }
    }
    catch (const exception &e) {
        cout << "Error while getVolInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getFsInfo(FsInfo &fsInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, block_count, block_size, " <<
                             "first_block, first_inum, root_inum, ftype, offset_fs " <<
                             "FROM fs_info WHERE id = " << id << ";");

        if (res.size() == 1) {
            int i = 0;
            res[0][i++].to(fsInfo.id);
            res[0][i++].to(fsInfo.block_count);
            res[0][i++].to(fsInfo.block_size);
            res[0][i++].to(fsInfo.first_block);
            res[0][i++].to(fsInfo.first_inum);
            res[0][i++].to(fsInfo.root_inum);
            res[0][i++].to((int &)fsInfo.type);
            res[0][i++].to(fsInfo.offset);
        }
        else {
            rowNumberException e;
            throw e;
        }
    }
    catch (const exception &e) {
        cout << "Error while getFsInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getFileInfo(FileInfo &fileInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, fs_id, name, " <<
                             "atime, ctime, mtime, crtime, meta_addr, content_len, " <<
                             "flags, mode, type, uid, gid, size, path, hash " <<
                             "FROM files_info WHERE id = " << id << ";");

        if (res.size() == 1) {
            int i = 0;
            res[0][i++].to(fileInfo.id);
            res[0][i++].to(fileInfo.fs_id);
            res[0][i++].to(fileInfo.name);
            res[0][i++].to(fileInfo.atime);
            res[0][i++].to(fileInfo.ctime);
            res[0][i++].to(fileInfo.mtime);
            res[0][i++].to(fileInfo.crtime);
            res[0][i++].to(fileInfo.meta_addr);
            res[0][i++].to(fileInfo.content_len);
            res[0][i++].to((int &)fileInfo.flags);
            res[0][i++].to((int &)fileInfo.mode);
            res[0][i++].to((int &)fileInfo.type);
            res[0][i++].to(fileInfo.uid);
            res[0][i++].to(fileInfo.gid);
            res[0][i++].to(fileInfo.size);
            res[0][i++].to(fileInfo.path);
            res[0][i++].to(fileInfo.hash);
        }
        else {
            rowNumberException e;
            throw e;
        }
    }
    catch (const exception &e) {
        cout << "Error while getFileInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getObjectRelationInfo() const
{
    if (!isConnected())
        return 1;

    return 0;
}

int DBParser::getChildrenIds(long parent_id, vector<pair<long, DB_OBJECT_TYPE> > &ids)
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, type FROM objects_relation " <<
                             "WHERE parent_id = " << parent_id << " ORDER BY id ASC;");

        for (result::size_type i = 0; i < res.size(); ++i) {
            const result::tuple row = res[i];
            ids.push_back(make_pair(row[0].as<long>(), (DB_OBJECT_TYPE)row[1].as<int>()));
        }
    }
    catch (const exception &e) {
        cout << "Error while getChildrenIds: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getSettingsInfo(SettingsInfo &settingsInfo) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "SELECT id, max_view_size from db_settings;");
        if (res.size() == 1) {
            res[0][0].to(settingsInfo.id);
            res[0][1].to(settingsInfo.max_view_size);
        }
    }
    catch (const exception &e) {
        cout << "Error while getSettingsInfo: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getTLRecordsNum() const
{
    read_transaction tr(*_con);
    result res = tr.exec(MakeQuery() << "select count(*) from timeline;");

    return res[0][0].as<int>();
}

int DBParser::getTLRecord(TimeLineData &TLInfo, int offset) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select file_id, time, name, event " <<
                             "from timeline where id = " << offset + 1 << ";"); // id in database starts from 1
        if (res.size() == 1) {
            for (int i = 0; i < TimeLineData::size; ++i) {
                res[0][i].to(TLInfo.data[i]);
            }
            TLInfo.row = offset;
        }
    }
    catch (const exception &e) {
        cout << "Error while getTLRecord: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getFileLocationInFs(FileLocationInfo &fileLocationInfo, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select meta_addr, attr_type, attr_id, " <<
                                            "fs_info.offset_fs from files_info, " <<
                                            "fs_info where files_info.id = " << id <<
                                            " and fs_info.id = files_info.fs_id;");

        if (res.size() == 1) {
            res[0][0].to(fileLocationInfo.metaAddr);
            res[0][1].to(fileLocationInfo.attrType);
            res[0][2].to(fileLocationInfo.attrId);
            res[0][3].to(fileLocationInfo.fsOffset);
        }
        else {
            cout << "Not a file system file" << endl;
            return 1;
        }
    }
    catch (const exception &e) {
        cout << "Error while getFileLocationInFs: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getFileSize(TSK_OFF_T &size, long id) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select size from files_info where " <<
                             "id = " << id << ";");

        if (res.size() == 1) {
            res[0][0].to(size);
        }
    }
    catch (const exception &e) {
        cout << "Error while getFileSize: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getHistoryRecordsNum() const
{
    read_transaction tr(*_con);
    result res = tr.exec(MakeQuery() << "select count(*) from history_table;");

    return res[0][0].as<int>();
}

int DBParser::getHistoryRecord(HistoryData &historyInfo, int offset) const
{
    if (!isConnected())
        return 1;

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select id, db_name, img_path " <<
                             "from history_table"  << " offset " << offset << " limit 1;");
        if (res.size() == 1) {
            for (int i = 0; i < HistoryData::size; ++i) {
                res[0][i].to(historyInfo.data[i]);
            }
        }
        historyInfo.row = offset;
    }
    catch (const exception &e) {
        cout << "Error while getHistoryRecord: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::removeHistoryRow(int id) const
{
    if (!isConnected())
        return 1;

    try {
        work w(*_con);
        w.exec(MakeQuery() << "delete from history_table where id = " << id << ";");
        w.commit();
    }
    catch (const exception &e) {
        cout << "Error while removeHistoryRow: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::truncateSearchTable()
{
    if (!isConnected()) {
        return 1;
    }
    try {
        work w(*_con);
        string truncate_query = MakeQuery() << "truncate search_results;";
        w.exec(truncate_query);
        w.commit();
    }
    catch(const exception &e) {
        cout << "Error while truncate search table: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getNewSearch(QString params)
{
    if (!isConnected()) {
        return 1;
    }

    try {
        if (truncateSearchTable() != 0) {
            throw exception();
        }

        work w(*_con);
        string insert_query = MakeQuery() << "insert into search_results(id, file_id, name) "
                                          << "select row_number() over (order by id, name) "
                                          << "number, id, name from files_info where "
                                          << params.toStdString() << ";";
        w.exec(insert_query);
        w.commit();
    }
    catch(const exception &e) {
        cout << "Error while getNewSearch: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int DBParser::getSearchResultSize() const
{
    if (!isConnected()) {
        return 0;
    }

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select count(*) from search_results;");
        return res[0][0].as<int>();
    }
    catch(const exception &e) {
        cout << "Error while getSearchResultSize: " << e.what() << endl;
    }

    return 0;
}

int DBParser::getSearchResultRecord(SearchData &searchInfo, int offset) const
{
    if (!isConnected()) {
        return 1;
    }

    try {
        read_transaction tr(*_con);
        result res = tr.exec(MakeQuery() << "select file_id, name " <<
                             "from search_results where id = " << offset + 1); // id in database starts from 1
        if (res.size() == 1) {
            for (int i = 0; i < SearchData::size; ++i) {
                res[0][i].to(searchInfo.data[i]);
            }
            searchInfo.row = offset;
        }
    }
    catch (const exception &e) {
        cout << "Error while getSearchResultRecord: " << e.what() << endl;
        return 1;
    }

    return 0;
}
