#include "db_creator.h"
#include "query_utils.h"
#include "data_prepare.h"

#include <iostream>
#include <ctime>

using namespace std;
using namespace pqxx;

DBCreator::DBCreator(string name) :
    _name(name),
    _useExistDataBase(false),
    _isConnected(false),
    _configReader(_configFile)
{
    parseConfigFile();
}

DBCreator::~DBCreator()
{
    disconnect();
}

void DBCreator::parseConfigFile()
{
    _configReader.read(_queueSize, "queueSize");
    _configReader.read(_maxViewSize, "maxViewSize");
    YAML::Node tables_node;
    _configReader.read(tables_node, "tables");

    if (!tables_node.IsDefined()) {
        cout << "Can not read tables node" << endl;
        throw;
    }

    for (size_t i = 0; i < tables_node.size(); ++i) {
        string name = tables_node[i]["name"].as<string>();
        YAML::Node cols_params = tables_node[i]["cols"];

        if (!cols_params.IsDefined()) {
            cout << "Can not read cols params node" << endl;
            throw;
        }

        vector<string> col_names;
        vector<string> col_types;
        for (size_t col = 0; col < cols_params.size(); ++col) {
            YAML::Node next_col = cols_params[col];
            assert(next_col.IsSequence()); // check that it is a sequence
            assert(next_col.size() == 2);  // and check that it consists of two fields (name, type)

            col_names.push_back(next_col[0].as<string>());
            col_types.push_back(next_col[1].as<string>());
        }

        _tables[name] = Table(col_types.size(), name, col_names, col_types);
    }

    YAML::Node indexes_node;
    _configReader.read(indexes_node, "indexes");

    if (!indexes_node.IsDefined()) {
        cout << "Can not read indexes node" << endl;
        throw;
    }

    for (size_t i = 0; i < indexes_node.size(); ++i) {
        _indexes.emplace_back(
                indexes_node[i]["name"].as<string>(),
                indexes_node[i]["table"].as<string>(),
                indexes_node[i]["col"].as<string>()
            );
    }
}

int DBCreator::connect()
{
    bool isExist = checkMatches(_name);
    if (isExist && !_useExistDataBase) {
        // Database with such name already exists
        // continue with this name or choose new one
        return 1;
    }
    _con = new connection(MakeQuery() << "dbname = " << _name << " user = postgres");

    if (_con->is_open()) {
        _isConnected = true;
        cout << _name << " is opened" << endl;
    }

    if (isExist && _useExistDataBase) {
        if (cleanDataBase())
            cout << "cleanDataBase failed" << endl;
    }

    createTables();
    createIndexes();

    fillSettingsTable();

    return 0;
}

int DBCreator::disconnect()
{
    if (_isConnected) {
        _isConnected = false;
        _con->disconnect();
        delete _con;
    }

    return 0;
}

int DBCreator::checkMatches(string name) const
{
    string con_args = "dbname = template1 user = postgres";
    connection check_con(con_args);
    nontransaction tran(check_con);

    result res = tran.exec(MakeQuery() << "select count(*) from pg_catalog.pg_database where datname = "
                                       << tran.quote<string>(name));
    if (res.size() == 0 || res[0][0].as<int>() == 0) {
        cout << name << " not exists" << endl;
        tran.exec(MakeQuery() << "create database \"" << name << "\" with owner = postgres");
        return 0;
    }
    else {
        cout << name << " exists" << endl;
        return 1;
    }
}

int DBCreator::createTables()
{
    work w(*_con);
    for (const auto &t : _tables)
        createTable(w, t.second);
    
    w.commit();

    return 0;
}

int DBCreator::createIndexes()
{
    work w(*_con);
    for (const Index &idx: _indexes)
        createIndex(w, idx);

    w.commit();

    return 0;
}

int DBCreator::getFileInfoSize() const
{
    nontransaction tr(*_con);
    result res = tr.exec(MakeQuery() << "select count(*) from files_info;");

    return res[0][0].as<int>();
}

void DBCreator::createTimeLine()
{
    stringstream create_view_query;
    create_view_query << "create view timeline_view as "
               << "select id, atime as time, name, 'access' as event from files_info union all "
               << "select id, crtime, name, 'create' from files_info union all "
               << "select id, ctime, name, 'change' from files_info union all "
               << "select id, mtime, name, 'modified' from files_info order by time asc;";
    executeQuery(create_view_query.str());

    stringstream update_table_query;
    update_table_query << "insert into timeline (file_id, time, name, event) "
                       << "select * from timeline_view order by time;";
    executeQuery(update_table_query.str());

    stringstream delete_view_query;
    delete_view_query << "drop view timeline_view;";
    executeQuery(delete_view_query.str());
}

int DBCreator::cleanDataBase()
{
    work w(*_con);

    for (const auto &t: _tables)
        dropTable(w, t.first);
    for (const auto &idx: _indexes)
        dropIndex(w, idx.name);

    w.commit();

    return 0;
}

void DBCreator::executeQuery(string query)
{
    try {
        work w(*_con);
        w.exec(query);
        w.commit();
    }
    catch (const exception &e) {
        cout << "executeQuery Exception: " << e.what() << endl;
        cout << query << endl;
    }
}

size_t DBCreator::getQueueSize()
{
    return _queryQueue.size();
}

size_t DBCreator::getMaxViewSize()
{
    return _maxViewSize;
}

void DBCreator::addQueryToQueue(string query)
{
    _queryQueue.push(query);

    if (_queryQueue.size() == _queueSize)
        executeAllQueue();
}

void DBCreator::executeAllQueue()
{
    try {
        work w(*_con);
        while (!_queryQueue.empty()) {
            w.exec(_queryQueue.front());
            _queryQueue.pop();
        }
        w.commit();
    }
    catch (const exception &e) {
        cout << "executeAllQueue Exception: " << e.what() << endl;
    }
}

void DBCreator::createTable(work &w, const Table &t)
{
    stringstream query;

    query << "create table " << t.name << " (";
    for (unsigned int i = 0; i < t.size; ++i) {
        query << " " << t.col_names[i] <<
                 " " << t.col_types[i] <<
                 ", " [i == t.size - 1];
    }
    query << ");";

    w.exec(query.str());
}

void DBCreator::dropTable(work &w, string table_name)
{
    result res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
        w.quote<string>(table_name) << " and relkind = " << w.quote<string>("r") << " );");
    if (res[0][0].as<string>() == "t")
        w.exec(MakeQuery() << "drop table " << table_name << ";");
}

void DBCreator::createIndex(pqxx::work &w, const Index &idx)
{
    stringstream query;
    query << "create index " << idx.name << " ON " << idx.table << " (" << idx.col << ");";

    w.exec(query.str());
}

void DBCreator::dropIndex(pqxx::work &w, std::string index_name)
{
    result res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
        w.quote<string>(index_name) << " and relkind = " << w.quote<string>("i") << " );");
    if (res[0][0].as<string>() == "t")
        w.exec(MakeQuery() << "drop index " << index_name << ";");
}

string DBCreator::getName() const
{
    return _name;
}

void DBCreator::setName(string name)
{
    _name = name;
}

void DBCreator::allowUseExistDataBase(bool flag)
{
    _useExistDataBase = flag;
}

int DBCreator::isTableExist(string name)
{
    if (!_tables.count(name)) {
        cout << "Table with name " << name << " was not found." << endl;
        return 1;
    }

    return 0;
}

void DBCreator::fillSettingsTable()
{
    insertInTable(_tables["db_settings"], "DEFAULT", _maxViewSize);
}

int DBCreator::addImgInfo(int id, const TSK_IMG_INFO* img_info, int num, const char* const images[])
{
    if (addObjectsRelation(id, 0, DB_OBJECT_TYPE_IMAGE))
        return 1;

    return insertInTable(_tables["img_info"], id,
                                              screenQuotes(string(images[0])),
                                              screenQuotes(cutPath(string(images[0]))),
                                              img_info->itype,
                                              img_info->sector_size,
                                              img_info->size);
}

int DBCreator::addVolInfo(int id, int parent_id, const TSK_VS_PART_INFO* vs_part)
{
    if (addObjectsRelation(id, parent_id, DB_OBJECT_TYPE_VOL))
        return 1;

    return insertInTable(_tables["vol_info"], id,
                                              vs_part->addr,
                                              vs_part->desc,
                                              vs_part->flags,
                                              vs_part->len,
                                              vs_part->start);
}

int DBCreator::addVolSysInfo(int id, int parent_id, const TSK_VS_INFO* vs_info)
{
    if (addObjectsRelation(id, parent_id, DB_OBJECT_TYPE_VS))
        return 1;

    return insertInTable(_tables["vs_info"], id,
                                             vs_info->block_size,
                                             vs_info->endian,
                                             vs_info->offset,
                                             vs_info->part_count,
                                             vs_info->vstype);
}

int DBCreator::addFileSysInfo(int id, int parent_id, TSK_FS_INFO* fs_info)
{
    if (addObjectsRelation(id, parent_id, DB_OBJECT_TYPE_FS))
        return 1;

    return insertInTable(_tables["fs_info"], id,
                                             fs_info->block_count,
                                             fs_info->block_size,
                                             fs_info->first_block,
                                             fs_info->first_inum,
                                             fs_info->last_inum,
                                             fs_info->root_inum,
                                             fs_info->ftype,
                                             fs_info->offset);
}

int DBCreator::addFileInfo(int id, int fs_id, int parent_id, TSK_FS_FILE* fs_file,
                           const TSK_FS_ATTR* fs_attr, const char* path, std::string hash)
{
    if (addObjectsRelation(id, parent_id, DB_OBJECT_TYPE_FILE))
        return 1;

    int attrId = 0;
    int attrType = TSK_FS_ATTR_TYPE_NOT_FOUND;

    if (fs_attr) {
        attrId = fs_attr->id;
        attrType = fs_attr->type;
    }

    return insertInTable(_tables["files_info"], id,
                                                fs_id,
                                                screenQuotes(string(fs_file->name->name)),
                                                toTimeFormat(fs_file->meta->atime),
                                                toTimeFormat(fs_file->meta->ctime),
                                                toTimeFormat(fs_file->meta->mtime),
                                                toTimeFormat(fs_file->meta->crtime),
                                                fs_file->name->meta_addr,
                                                attrId,
                                                attrType,
                                                fs_file->meta->content_len,
                                                fs_file->meta->flags,
                                                fs_file->meta->mode,
                                                fs_file->meta->type,
                                                fs_file->meta->uid,
                                                fs_file->meta->gid,
                                                fs_file->meta->size,
                                                screenQuotes(path),
                                                hash);
}

int DBCreator::addObjectsRelation(int id, int parent_id, DB_OBJECT_TYPE type)
{
    return insertInTable(_tables["objects_relation"], id, parent_id, type);
}

template <typename T>
string DBCreator::getVal(stringstream &ss, const Table &table, int &val_id, T val)
{
    if (table.col_types[val_id] == "TEXT")
        ss << "\'" << val << "\'";
    else
        ss << val;

    return ss.str();
}

template <typename Head, typename... Tail>
string DBCreator::getVal(stringstream &ss, const Table &table, int &val_id, Head head, Tail... tail)
{
    getVal(ss, table, val_id, head);
    ss << ", ";
    val_id = table.size - sizeof...(tail);
    getVal(ss, table, val_id, tail...);
    
    return ss.str();
}

template <typename... Args>
int DBCreator::insertInTable(const Table &table, Args... args)
{
    if (isTableExist(table.name))
        return 1;

    if (table.size != sizeof...(args))
        return 1;

    stringstream ss;
    ss << "insert into " << table.name << " values (";
    int val_id = 0;
    string res = getVal(ss, table, val_id, args...);
    ss.str("");
    ss << res << ");";
    addQueryToQueue(ss.str());

    return 0;
}

void DBCreator::updateHistory(std::string db_name, std::string img_path)
{
    string historyBD = "history_db";
    string historyTable = "history_table";
    bool isHistoryExists = checkMatches(historyBD);

    shared_ptr<connection> historyConnection (new connection(MakeQuery() << "dbname = " <<
                                                              historyBD << " user = postgres"));
    if (!historyConnection.get()->is_open()) {
        cout << "Failed while open history data base" << endl;
        return;
    }

    if (!isHistoryExists) {
        try {
            work w(*(historyConnection.get()));
            vector<string> col_names = { "id", "db_name", "img_path" };
            vector<string> col_types = { "SERIAL PRIMARY KEY", "TEXT", "TEXT" };
            createTable(w, Table(col_names.size(), historyTable, col_names, col_types));
            w.commit();
        }
        catch (const exception &e) {
            cout << "Failed while create history table: " << e.what() << endl;
            return;
        }
    }

    try {
        work w(*(historyConnection.get()));
        result res = w.exec(MakeQuery() << "select count(*) from " << historyTable <<
                            " where db_name = " << w.quote<string>(db_name) << ";");

        if (res.size() == 1) {
            w.exec(MakeQuery() << "delete from " << historyTable << " where db_name = " <<
                   w.quote<string>(db_name) << ";");
        }

        w.exec(MakeQuery() << "insert into " << historyTable << " values (" <<
               "DEFAULT, " << w.quote<string>(db_name) << ", " <<
               w.quote<string>(img_path) << ");");
        w.commit();
    }
    catch (const exception &e) {
        cout << "Failed while insert in cases_history: " << e.what() << endl;
    }
}
