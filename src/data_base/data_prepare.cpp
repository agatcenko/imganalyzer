#include "data_prepare.h"

#include <sstream>

using namespace std;

string toTimeFormat(time_t time)
{
    string format_str = "%Y/%m/%d %H:%M:%S";
    string time_str;

    struct tm *time_info;
    time_info = localtime(&time);
    
    int len = 0, i = 1;
    while (len == 0) {
        time_str.resize(i * format_str.size());
        len = strftime(&time_str[0], time_str.size(), format_str.c_str(), time_info);
        ++i;
    }

    time_str.resize(len);
    stringstream ss;
    ss << "\'" << time_str << "\'";
    
    return ss.str();
}

string screenQuotes(string text)
{
    string res;

    char quote = '\'';
    size_t start_pos = 0, found_pos = 0;
    while (found_pos != string::npos) {
        found_pos = text.find(quote, start_pos);
        if (found_pos == string::npos)
            res += text.substr(start_pos, text.size() - start_pos + 1);
        else {
            res += text.substr(start_pos, found_pos - start_pos + 1);
            res += quote;
        }
        start_pos = found_pos + 1;
    }

    return res;
}

string cutPath(const string &path)
{
    string path_str = path;
    size_t path_separator = path_str.find_last_of("/\\");
    path_str = path_str.substr(path_separator + 1, path.size() - path_separator - 1);

    return path_str;
}
