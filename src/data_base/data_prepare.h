#pragma once

#include <string>

std::string toTimeFormat(time_t time);
std::string screenQuotes(std::string text);
std::string cutPath(const std::string &path);
