#pragma once

#include <ctime>
#include <array>
#include <string>
#include <exception>
#include <tsk/libtsk.h>

#include <QVariant>

struct rowNumberException : std::exception {
    const char* what() const noexcept { return "Unexpected number of rows"; }
};

enum DB_OBJECT_TYPE {
    DB_OBJECT_TYPE_IMAGE = 0,
    DB_OBJECT_TYPE_VS,
    DB_OBJECT_TYPE_VOL,
    DB_OBJECT_TYPE_FS,
    DB_OBJECT_TYPE_FILE
};

struct ImageInfo
{
    int id;
    std::string path;
    std::string name;
    std::string type;
    long size;
    long sectorSize;
};

struct VsInfo
{
    int id;
    unsigned int block_size;
    TSK_ENDIAN_ENUM endian;
    TSK_DADDR_T offset_vs;
    TSK_PNUM_T part_count;
    TSK_VS_TYPE_ENUM vstype;
};

struct VolInfo
{
    int id;
    TSK_PNUM_T addr;
    std::string desc;
    TSK_DADDR_T len;
    TSK_DADDR_T start;
    TSK_VS_PART_FLAG_ENUM flags;

};

struct FsInfo
{
    int id;
    TSK_DADDR_T block_count;
    unsigned int block_size;
    TSK_DADDR_T first_block;
    TSK_INUM_T first_inum;
    TSK_INUM_T last_inum;
    TSK_INUM_T root_inum;
    TSK_FS_TYPE_ENUM type;
    TSK_OFF_T offset;
};

struct FileInfo
{
    long id;
    int fs_id;
    std::string name;
    std::string atime;
    std::string ctime;
    std::string mtime;
    std::string crtime;
    size_t content_len;
    TSK_INUM_T meta_addr;
    TSK_FS_META_FLAG_ENUM flags;
    TSK_FS_META_MODE_ENUM mode;
    TSK_FS_META_TYPE_ENUM type;
    TSK_UID_T uid;
    TSK_GID_T gid;
    TSK_OFF_T size;
    std::string path;
    std::string hash;
};

struct ObjectRelationInfo
{
    long id;
    long parent_id;
    int type;
};

struct SettingsInfo
{
    int id;
    int max_view_size;
};

template <size_t N>
struct TableContainer
{
    static const int size = N;

    int row = -1;
    std::array<std::string, size> data;

    QVariant at(int i)
    {
        if (i < 0 || i >= size)
            return QVariant();

        return QString::fromStdString(data[i]);
    }
};

using SearchData = TableContainer<2>;
using HistoryData = TableContainer<3>;
using TimeLineData = TableContainer<4>;

struct FileLocationInfo
{
    TSK_OFF_T fsOffset;
    TSK_INUM_T metaAddr;
    int attrType;
    int attrId;
};

std::string metaFlagEnumToString(TSK_FS_META_FLAG_ENUM flag);
std::string metaModeEnumToString(TSK_FS_META_MODE_ENUM mode);
std::string metaTypeEnumToString(TSK_FS_META_TYPE_ENUM type);
