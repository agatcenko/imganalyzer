#pragma once

#include "db_types.h"

#include <pqxx/pqxx>
#include <string>
#include <vector>
#include <utility>

class DBParser {
public:
    DBParser();
    ~DBParser();

    int connect();
    void disconnect();

    void setDBName(std::string name);
    std::string getDBName() const;

    int getImageInfo(ImageInfo &imageInfo, long id) const;
    int getVsInfo(VsInfo &vsInfo, long id) const;
    int getVolInfo(VolInfo &volInfo, long id) const;
    int getFsInfo(FsInfo &fsInfo, long id) const;
    int getFileInfo(FileInfo &fileInfo, long id) const;
    int getObjectRelationInfo() const;
    int getSettingsInfo(SettingsInfo &settingsInfo) const;
    int getChildrenIds(long parent_id, std::vector<std::pair<long, DB_OBJECT_TYPE> > &ids);
    int getFileLocationInFs(FileLocationInfo &fileLocationInfo, long id) const;
    int getFileSize(TSK_OFF_T &size, long id) const;

    // TL = TimeLine
    int getTLRecordsNum() const;
    int getTLRecord(TimeLineData &TLInfo, int offset) const;

    int getHistoryRecordsNum() const;
    int getHistoryRecord(HistoryData &historyInfo, int offset) const;
    int removeHistoryRow(int id) const;

    int truncateSearchTable();
    int getNewSearch(QString params);
    int getSearchResultSize() const;
    int getSearchResultRecord(SearchData &searchInfo, int offset) const;

private:
    std::string _name;
    pqxx::connection* _con = nullptr;

    SettingsInfo _settings;

    bool isConnected() const;
};
