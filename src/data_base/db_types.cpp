#include "db_types.h"

using namespace std;

string metaFlagEnumToString(TSK_FS_META_FLAG_ENUM flag)
{
    if (flag & TSK_FS_META_FLAG_ALLOC)
        return "Allocated";
    if (flag & TSK_FS_META_FLAG_UNALLOC)
        return "Unallocated";
    if (flag & TSK_FS_META_FLAG_USED)
        return "Used";
    if (flag & TSK_FS_META_FLAG_UNUSED)
        return "Unused";
    if (flag & TSK_FS_META_FLAG_COMP)
        return "Compressed";
    if (flag & TSK_FS_META_FLAG_ORPHAN)
        return "Orphan";

    return string();
}

string metaModeEnumToString(TSK_FS_META_MODE_ENUM mode)
{
    string mode_str = "---------";

    if (mode & TSK_FS_META_MODE_IRUSR) mode_str[0] = 'r';
    if (mode & TSK_FS_META_MODE_IWUSR) mode_str[1] = 'w';
    if (mode & TSK_FS_META_MODE_IXUSR) mode_str[2] = 'x';

    if (mode & TSK_FS_META_MODE_IRGRP) mode_str[3] = 'r';
    if (mode & TSK_FS_META_MODE_IWGRP) mode_str[4] = 'w';
    if (mode & TSK_FS_META_MODE_IXGRP) mode_str[5] = 'x';

    if (mode & TSK_FS_META_MODE_IROTH) mode_str[6] = 'r';
    if (mode & TSK_FS_META_MODE_IWOTH) mode_str[7] = 'w';
    if (mode & TSK_FS_META_MODE_IXOTH) mode_str[8] = 'x';

    return mode_str;
}

string metaTypeEnumToString(TSK_FS_META_TYPE_ENUM type)
{
    if (type & TSK_FS_META_TYPE_UNDEF)
        return "Undefined";
    if (type & TSK_FS_META_TYPE_REG)
        return "Regular file";
    if (type & TSK_FS_META_TYPE_DIR)
        return "Directory file";
    if (type & TSK_FS_META_TYPE_FIFO)
        return "Named pip (fifo)";
    if (type & TSK_FS_META_TYPE_CHR)
        return "Character device";
    if (type & TSK_FS_META_TYPE_BLK)
        return "Block device";
    if (type & TSK_FS_META_TYPE_LNK)
        return "Symbolic link";
    if (type & TSK_FS_META_TYPE_SHAD)
        return "SOLARIS Only";
    if (type & TSK_FS_META_TYPE_SOCK)
        return "UNIX domain socket";
    if (type & TSK_FS_META_TYPE_WHT)
        return "Whiteout";

    return string();
}
