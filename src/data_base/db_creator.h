#pragma once

#include "db_types.h"
#include "../config_utils/yaml_reader.h"

#include <tsk/libtsk.h>
#include <pqxx/pqxx>

#include <string>
#include <vector>
#include <queue>
#include <map>


struct Table
{
    unsigned int size;
    std::string name;
    std::vector<std::string> col_names;
    std::vector<std::string> col_types;

    Table(unsigned int _size = 0, std::string _name = "",
        std::vector<std::string> _names = std::vector<std::string>(),
        std::vector<std::string> _types = std::vector<std::string>())
    {
        size = _size;
        name = _name;
        col_names = _names;
        col_types = _types;
    }
};

struct Index {
    std::string name;
    std::string table;
    std::string col;

    Index(std::string _name = "", std::string _table = "", std::string _col = "") :
        name(_name), table(_table), col(_col)
    {
    }
};

class DBCreator
{
public:
    DBCreator(std::string name = "default");
    ~DBCreator();

    int connect();
    int disconnect();

    int checkMatches(std::string name) const;
    int createTables();
    int createIndexes();
    void createTimeLine();

    int cleanDataBase();

    void executeAllQueue();
    void addQueryToQueue(std::string query);
    std::size_t getQueueSize();
    std::size_t getMaxViewSize();

    void executeQuery(std::string query);
    void createTable(pqxx::work &w, const Table &t);
    void dropTable(pqxx::work &w, std::string table_name);

    void createIndex(pqxx::work &w, const Index &idx);
    void dropIndex(pqxx::work &w, std::string index_name);

    std::string getName() const;
    void setName(std::string name);
    void allowUseExistDataBase(bool flag);

    int getFileInfoSize() const;

    void fillSettingsTable();
    int addImgInfo(int id, const TSK_IMG_INFO* img_info, int num, const char* const images[]);
    int addVolInfo(int id, int parent_id, const TSK_VS_PART_INFO* vs_part);
    int addVolSysInfo(int id, int parent_id, const TSK_VS_INFO* vs_info);
    int addFileSysInfo(int id, int parent_id, TSK_FS_INFO* fs_info);
    int addFileInfo(int id, int fs_id, int parent_id, TSK_FS_FILE* fs_file,
                    const TSK_FS_ATTR* fs_attr, const char* path, std::string hash);
    int addObjectsRelation(int id, int parent_id, DB_OBJECT_TYPE type);

    void updateHistory(std::string db_name, std::string img_path);

private:
    std::string _name;
    pqxx::connection* _con;

    std::map<std::string, Table> _tables;
    std::vector<Index> _indexes;

    std::size_t _maxViewSize;
    std::size_t _queueSize;
    std::queue<std::string> _queryQueue;

    bool _useExistDataBase;
    bool _isConnected;

    const std::string _configFile = "config/data_base.yml";
    YamlReader _configReader;

    void parseConfigFile();
    int isTableExist(std::string name);

    template <typename T>
    std::string getVal(std::stringstream &ss, const Table &table, int &val_id, T val);

    template <typename Head, typename... Tail>
    std::string getVal(std::stringstream &ss, const Table &table, int &val_id, Head head, Tail... tail);

    template <typename... Args>
    int insertInTable(const Table &table, Args... args);
};
